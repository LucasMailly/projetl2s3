package analyzer.ij.plugin;

//import java.awt.*;
//import java.awt.image.*;
//import java.io.*;
//import analyzer.ij.io.*;
//import analyzer.ij.process.*;


/** This plugin displays the contents of a text file in a window. */
public class TextFileReader implements PlugIn {
	
	public void run(String arg) {
		new analyzer.ij.text.TextWindow(arg,400,450);
	}

}
