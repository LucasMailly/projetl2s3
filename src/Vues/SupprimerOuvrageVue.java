package Vues;

import java.sql.SQLException;

import Controllers.SupprimerOuvrageController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SupprimerOuvrageVue {
	private HomeVue homeVue;
	private SupprimerOuvrageController supprimerOuvrageCtrl;
	@FXML
    Button btnCancel, btnDelete;
	
	@FXML
	TextField tfDelete;
	
	public SupprimerOuvrageVue(HomeVue homeVue, SupprimerOuvrageController supprimerOuvrageCtrl) {
		this.homeVue = homeVue;
		this.supprimerOuvrageCtrl = supprimerOuvrageCtrl;
	}
	 
	@FXML
	private void btnDeleteClicked() throws SQLException {
		if(tfDelete.getText().equals("SUPPRIMER")) {
			supprimerOuvrageCtrl.supprimerOuvrage();
			((Stage) tfDelete.getScene().getWindow()).close();
			homeVue.updateOuvrages();
		}
	}
	
	@FXML
	private void btnCancelClicked() {
		((Stage) tfDelete.getScene().getWindow()).close();
	}
}

