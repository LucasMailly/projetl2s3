package Vues;

import Controllers.ImgViewerController;
import Modeles.Composante;
import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Classe ImgViewerVue
 * G�re le contenu du canvas qui contient l'image affich�e en mode analyse ou non.
 * @author MAILLY Lucas
 */
public class ImgViewerVue {

    private final int CANVA_WIDTH;
    private final int CANVA_HEIGHT;
    private final ImgViewerController imgViewerCtrl;
    private final VisualiserVue visualiserVue;
    private Canvas canvas;
    private GraphicsContext g;
    private Point2D origine = new Point2D(0,0);
    private Point2D mousePos = new Point2D(0,0);
    private double zoomCoeff = 1.1;
    private double width=0;
    private double height=0;
    private double initialWidth;
    private double initialHeight;
    private Image image;
    private boolean analyse;
    private HashMap<BoundingBox, Composante> bbs=new HashMap<>();
    private Composante selectedCC;
    private Composante clickedCC;
    private Point2D mousePressedPosition = new Point2D(-1,-1);

    /**
     * Constructeur de ImgViewerVue qui initialise les dimensions du canvas et si on souhaite le mode analyse.
     * Le constructeur appelle automatiquement les fonctions d'initialisation des Listeners et met � jour l'image.
     * @param CANVA_WIDTH
     * @param CANVA_HEIGHT
     * @param analyse
     * @param imgViewerCtrl
     * @see #setListeners()
     */
    public ImgViewerVue(int CANVA_WIDTH, int CANVA_HEIGHT, boolean analyse, ImgViewerController imgViewerCtrl, VisualiserVue visualiserVue) {
        this.visualiserVue = visualiserVue;
        this.imgViewerCtrl = imgViewerCtrl;
        this.CANVA_WIDTH = CANVA_WIDTH;
        this.CANVA_HEIGHT = CANVA_HEIGHT;
        canvas = new Canvas(CANVA_WIDTH, CANVA_HEIGHT);
        g = canvas.getGraphicsContext2D();
        this.analyse=analyse;
        updateImage();
        setListeners();
    }

    /**
     *
     * @return le canvas de travail
     */
    public Canvas getCanvas() {
        return canvas;
    }

    /**
     * Met � jour l'origine du canvas selon les coordon�es du point pass� en param�tre
     * @param p
     */
    public void setOrigin(Point2D p){
        origine = new Point2D(p.getX(),p.getY());
        refresh();
    }

    /**
     *
     * @return le point correspondant � l'origine du canvas de travail
     */
    public Point2D getOrigin(){
        return origine;
    }
    public void setSize(double[] size){
        this.width=size[0];
        this.height=size[1];
        refresh();
    }

    /**
     * Retourne les dimensions de l'image affiché
     * @return
     */
    public double[] getSize(){
        return new double[]{width, height};
    }

    /**
     * Lance les Listeners pour :
     * Zoom
     * D�placer l'image
     * Centrer l'image
     * Afficher les Bounding boxes pour le mode analyse
     */
    private void setListeners() {

        //Zoom
        canvas.setOnScroll(e -> {
            if (!image.isError()) {
                double xOffset;
                double yOffset;
                if (e.getDeltaY() > 0 && width / initialWidth < 20) {
                    width *= zoomCoeff;
                    height *= zoomCoeff;
                    origine = origine.subtract((e.getX() - origine.getX()) * (zoomCoeff - 1), (e.getY() - origine.getY()) * (zoomCoeff - 1));
                }
                if (e.getDeltaY() < 0 && width / initialWidth > 0.1) {
                    width *= 1 / zoomCoeff;
                    height *= 1 / zoomCoeff;
                    origine = origine.add((e.getX() - origine.getX()) * (zoomCoeff - 1), (e.getY() - origine.getY()) * (zoomCoeff - 1));
                }
                refresh();
            }
        });

        //Move image
        canvas.setOnMousePressed(e -> {
            if (!image.isError()) {
                mousePos = new Point2D(e.getX(), e.getY());
                mousePressedPosition = new Point2D(e.getX(), e.getY());
                if (e.getClickCount() == 2) {
                    adjustImage();
                    centerImage();
                }
            }
        });

        canvas.setOnMouseDragged(e -> {
            if (!image.isError()) {
                Point2D newOrigine = new Point2D(origine.getX() + e.getX() - mousePos.getX(), origine.getY() + e.getY() - mousePos.getY());
                if (newOrigine.getX() < CANVA_WIDTH && newOrigine.getX() + width > 0)
                    origine = new Point2D(newOrigine.getX(), origine.getY());
                if (newOrigine.getY() < CANVA_HEIGHT && newOrigine.getY() + height > 0)
                    origine = new Point2D(origine.getX(), newOrigine.getY());
                mousePos = new Point2D(e.getX(), e.getY());
                refresh();
            }
        });

        //Select CC
        canvas.setOnMouseClicked(e -> {
            if (analyse && e.getX()==mousePressedPosition.getX() && e.getY()==mousePressedPosition.getY()){
                visualiserVue.setComposante(selectedCC);
                clickedCC=selectedCC;
            }
        });

        //Highlight BB
        canvas.setOnMouseMoved(e -> {
            if (analyse && !image.isError()) {
                refresh();
                mousePos = new Point2D(e.getX(),e.getY());
                double scale = width/image.getWidth();
                double xOffset = origine.getX();
                double yOffset = origine.getY();
                HashMap<BoundingBox, BoundingBox> selectedBBsAssociation = new HashMap<>();
                ArrayList<BoundingBox> selectedBBs= new ArrayList<>();
                for (BoundingBox bb : bbs.keySet()){
                    double x1 = bb.getMinX()*scale+xOffset;
                    double y1 = bb.getMinY()*scale+yOffset;
                    double bWidth = bb.getWidth()*scale;
                    double bHeight = bb.getHeight()*scale;
                    BoundingBox tmpBB = new BoundingBox(x1,y1,bWidth,bHeight);
                    if (tmpBB.intersects(mousePos.getX(),mousePos.getY(),0,0)) {
                        selectedBBsAssociation.put(tmpBB, bb);
                        selectedBBs.add(tmpBB);
                    }
                }
                if (!selectedBBs.isEmpty()) {
                    g.setFill(Color.BLUE);
                    selectedBBs= new ArrayList<>(selectedBBs.stream().sorted((o1, o2) -> {
                        if (o1.getWidth() != o2.getWidth()) {
                            if (o1.getWidth() < o2.getWidth())
                                return -1;
                            else
                                return 1;
                        } else {
                            return 0;
                        }
                    }).collect(Collectors.toList()));
                    BoundingBox b = selectedBBs.get(0);
                    selectedCC = bbs.get(selectedBBsAssociation.get(b));
                    double x1 = b.getMinX();
                    double y1 = b.getMinY();
                    double bWidth = b.getWidth();
                    double bHeight = b.getHeight();
                    g.fillRect(x1, y1, bWidth, 1);
                    g.fillRect(x1, y1, 1, bHeight);
                    g.fillRect(x1 + bWidth, y1, 1, bHeight);
                    g.fillRect(x1, y1 + bHeight, bWidth, 1);
                }else{
                    selectedCC = null;
                }
            }
        });

    }

    /**
     * Met � jour l'image du canvas
     */
    public void updateImage(){
        image = new Image("file:"+imgViewerCtrl.getImagePath());
        if (!image.isError()){
            //update image
            adjustImage();
            centerImage();
            initialWidth=width;
            initialHeight=height;
            refresh();

            //update BBs
            if (analyse){
                ArrayList<Composante> measures = imgViewerCtrl.getMeasures();
                bbs.clear();
                for (Composante c : measures){
                    bbs.put(new BoundingBox(c.getxStart(), c.getyStart(), c.getWidth(), c.getHeight()), c);
                }
            }
        }else{
            reset();
        }
    }

    public void setClickedCC(Composante CC){
        this.clickedCC = CC;
    }

    /**
     * Ajuste l'image au dimensions du canvas de travail
     */
    public void adjustImage(){
        if (image.getWidth()>image.getHeight()){
            width = canvas.getWidth();
            height = (image.getHeight()/image.getWidth())*width;
        }else{
            height = canvas.getHeight();
            width = (image.getWidth()/image.getHeight())*height;
        }
        refresh();
    }

    public void centerImage() {
        double xOffset = (origine.getX()+width/2)-canvas.getWidth()/2;
        double yOffset = (origine.getY()+height/2)-canvas.getHeight()/2;
        origine=origine.subtract(xOffset, yOffset);
        refresh();
    }

    /**
     * Met � jour le canvas (image et BoundingBoxes)
     */
    public void refresh() {
        if (!image.isError()) {
            g.clearRect(0, 0, CANVA_WIDTH, CANVA_HEIGHT);
            //Draw image
            g.drawImage(image, origine.getX(), origine.getY(), width, height);
            //Draw BBs
            if (analyse) {
                g.setFill(Color.RED);
                double scale = width / image.getWidth();
                double xOffset = origine.getX();
                double yOffset = origine.getY();
                for (BoundingBox b : bbs.keySet()) {
                    double x1 = b.getMinX() * scale + xOffset;
                    double y1 = b.getMinY() * scale + yOffset;
                    double bWidth = b.getWidth() * scale;
                    double bHeight = b.getHeight() * scale;
                    g.fillRect(x1, y1, bWidth, 1);
                    g.fillRect(x1, y1, 1, bHeight);
                    g.fillRect(x1 + bWidth, y1, 1, bHeight);
                    g.fillRect(x1, y1 + bHeight, bWidth, 1);
                }
                //color selectedCC in yellow
                if (clickedCC!=null){
                    BoundingBox selectedBB = new BoundingBox(clickedCC.getxStart(), clickedCC.getyStart(),
                            clickedCC.getWidth(),clickedCC.getHeight());
                    g.setFill(Color.YELLOW);
                    double x1 = selectedBB.getMinX() * scale + xOffset;
                    double y1 = selectedBB.getMinY() * scale + yOffset;
                    double bWidth = selectedBB.getWidth() * scale;
                    double bHeight = selectedBB.getHeight() * scale;
                    g.fillRect(x1, y1, bWidth, 1);
                    g.fillRect(x1, y1, 1, bHeight);
                    g.fillRect(x1 + bWidth, y1, 1, bHeight);
                    g.fillRect(x1, y1 + bHeight, bWidth, 1);
                }
            }
        }else{
            reset();
        }
    }

    /**
     * Efface tout le contenu du canvas
     */
    public void reset(){
        g.clearRect(0,0,CANVA_WIDTH,CANVA_HEIGHT);
    }

}
