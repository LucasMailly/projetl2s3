package Vues;

import Controllers.ImgViewerController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Classe AppVue
 * @author: MAILLY Lucas
 */
public class AppVue extends Application {
    /**
     * Ouvre la fen�tre initiale en mode non redimensionnable
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("home.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Text Analyzer");
        stage.getIcons().add(new Image("file:../../resources/icon.png"));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}