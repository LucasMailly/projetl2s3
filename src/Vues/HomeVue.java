package Vues;

import Controllers.AjoutOuvrageController;
import Controllers.HomeController;
import Controllers.ImgViewerController;
import Controllers.SupprimerOuvrageController;
import Controllers.VisualiserController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe HomeVue
 * @author MAILLY Lucas
 */
public class HomeVue {
    private HomeController homeCtrl = new HomeController(this);
    private double lastSearchTime = new Date().getTime();
    private double deltaSearch = 0.75;
    private String[] filters = {"titre","auteurNom","auteurPr�nom","auteurPseudo","imprimeur","editeur","annee","lieux"};

    @FXML
    private ComboBox comboBox;
    @FXML
    private VBox spVbox;
    @FXML
    private TextField searchBar;
    @FXML
    private Button btnAjoutOuvrage;


    /**
     * M�thode appel� automatiquement � l'ouverture de la fen�tre
     * - Ajoute les valeurs dans la comboBox g�rant le type de recherche
     * - Affiche tous les ouvrages stock�s en BDD
     */
    @FXML
    private void initialize() {
        comboBox.getItems().removeAll(comboBox.getItems());
        comboBox.getItems().addAll("Titre", "Nom d'auteur", "Pr�nom d'auteur", "Pseudo d'auteur", "Imprimeur", "Editeur", "Ann\u00e9e", "Lieu");
        comboBox.getSelectionModel().select("Titre");
        homeCtrl.afficheMenu();
        //searchBar listener
        searchBar.textProperty().addListener((obs, oldText, newText) -> {
            lastSearchTime = new Date().getTime();
            //call searchChanged() in 2 seconds
            Timeline timeline = new Timeline(
                new KeyFrame(Duration.millis(deltaSearch*1000), e -> {
                    searchChanged(newText);
                })
            );
            timeline.play();
        });
        //comboBox listener
        comboBox.valueProperty().addListener(e -> {
            homeCtrl.chercherOuvrage(searchBar.getText(), filters[comboBox.getSelectionModel().getSelectedIndex()]);
        });
    }

    private void searchChanged(String searchText){
        if (new Date().getTime()-lastSearchTime >= deltaSearch*1000) {
            homeCtrl.chercherOuvrage(searchText, filters[comboBox.getSelectionModel().getSelectedIndex()]);
        }
    }

    /**
     * Ouvre la fen�tre de cr�ation d'un ouvrage
     * @throws IOException
     */
    @FXML
    private void btnAjoutOuvrageClicked() {
        AjoutOuvrageController ajoutOuvrageCtrl = new AjoutOuvrageController(this);
        AjoutOuvrageVue ajoutOuvrageVue = ajoutOuvrageCtrl.getAjoutOuvrageVue();
        FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("ajoutOuvrage.fxml"));
        fxmlLoader.setController(ajoutOuvrageVue);
        try {
            Stage stage = new Stage();
            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Ajout d'un ouvrage");
            stage.getIcons().add(new Image("file:../../resources/icon.png"));
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear(){
        spVbox.getChildren().clear();
    }

    /**
     * Ajoute un ouvrage dans la vue HomeVue dans un GridPane avec les donn�es
     * pass�es en param�tre
     * @param titre
     * @param auteurNom
     * @param auteurPrenom
     * @param auteurPseudo
     * @param imprimeur
     * @param editeur
     * @param annee
     * @param nbPages
     * @param lieu
     * @param idOuvrage
     */
    public void AjouteOuvrage(String titre, String auteurNom, String auteurPrenom, String auteurPseudo, String imprimeur, String editeur, String annee, Integer nbPages, String lieu, int idOuvrage){
        int ouvrageByRowCount = 5;

        ArrayList<Label> labels = new ArrayList<Label>();
        Label lblTitre = new Label("Nom: "+titre);
        Tooltip lblTitreTooltip = new Tooltip(titre);
        lblTitre.setTooltip(lblTitreTooltip);
        labels.add(lblTitre);
        lblTitre.setStyle("-fx-font-weight: bold");
        Label lblAuteur = new Label("Auteur: " + auteurNom + " " + auteurPrenom + " (" + auteurPseudo + ")");
        Tooltip lblAuteurTooltip = new Tooltip(auteurNom + " " + auteurPrenom + " (" + auteurPseudo + ")");
        lblAuteur.setTooltip(lblAuteurTooltip);
        labels.add(lblAuteur);
        Label lblImprimimeur = new Label("Imprimeur: "+imprimeur);
        Tooltip lblImprimimeurTooltip = new Tooltip(imprimeur);
        lblImprimimeur.setTooltip(lblImprimimeurTooltip);
        labels.add(lblImprimimeur);
        Label lblEditeur = new Label("Editeur: "+editeur);
        Tooltip lblEditeurTooltip = new Tooltip(editeur);
        lblEditeur.setTooltip(lblEditeurTooltip);
        labels.add(lblEditeur);
        Label lblAnnee = new Label("Ann�e: "+annee);
        Tooltip lblAnneeTooltip = new Tooltip(annee);
        lblAnnee.setTooltip(lblAnneeTooltip);
        labels.add(lblAnnee);
        Label lblNbPages = new Label("NbPages: "+nbPages);
        Tooltip lblNbPagesTooltip = new Tooltip(String.valueOf(nbPages));
        lblNbPages.setTooltip(lblNbPagesTooltip);
        labels.add(lblNbPages);
        Label lblLieu = new Label("Lieu: "+lieu);
        Tooltip lblLieuTooltip = new Tooltip(lieu);
        lblLieu.setTooltip(lblLieuTooltip);
        labels.add(lblLieu);

        VBox delBtnVbox = new VBox();
        delBtnVbox.setAlignment(Pos.BOTTOM_RIGHT);
        Button delBtn = new Button("Supprimer");
        delBtnVbox.getChildren().add(delBtn);
        delBtn.setOnMouseClicked(e -> {
        	SupprimerOuvrageController supprimerOuvrageCtrl = new SupprimerOuvrageController(idOuvrage, this);
			SupprimerOuvrageVue supprimerOuvrageVue = new SupprimerOuvrageVue(this, supprimerOuvrageCtrl);
        	FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("supprimerOuvrage.fxml"));
            fxmlLoader.setController(supprimerOuvrageVue);
			try {
				Stage stage = new Stage();
	            Scene scene = new Scene(fxmlLoader.load());
	            stage.setTitle("Suppression d'un ouvrage");
	            stage.getIcons().add(new Image("file:../../resources/icon.png"));
	            stage.setScene(scene);
	            stage.initModality(Modality.APPLICATION_MODAL);
	            stage.show();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		});
        
        VBox ouvrageVbox = new VBox();
        ouvrageVbox.setStyle("-fx-background-color: #ffffff;-fx-border-color: black");
        ouvrageVbox.getChildren().addAll(labels);
        ouvrageVbox.getChildren().add(delBtnVbox);
        ouvrageVbox.setAlignment(Pos.CENTER);
        ouvrageVbox.setSpacing(5);
        ouvrageVbox.setPadding(new Insets(5));
        ouvrageVbox.setOnMouseEntered(e -> ouvrageVbox.setStyle("-fx-background-color: #eeeeee;-fx-border-color: black"));
        ouvrageVbox.setOnMouseExited(e -> ouvrageVbox.setStyle("-fx-background-color: #ffffff;-fx-border-color: black"));
        int ouvrageVboxWidth = 150;
        ouvrageVbox.setMaxWidth(ouvrageVboxWidth);
        //Ouvrage clicked listener
        ouvrageVbox.setOnMouseClicked(e -> {
            ImgViewerController imgViewerCtrl = new ImgViewerController();
            VisualiserController visualiserCtrl = new VisualiserController(idOuvrage, imgViewerCtrl);
            VisualiserVue visualiserVue = visualiserCtrl.getVisualiserVue();
            FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("visualiser.fxml"));
            fxmlLoader.setController(visualiserVue);
            try {
                Scene scene = new Scene(fxmlLoader.load());
                Stage window = (Stage) searchBar.getScene().getWindow();
                window.setScene(scene);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        for (Label l : labels){
            l.setMinWidth(ouvrageVboxWidth);
            l.setMaxWidth(ouvrageVboxWidth);
        }
        
        delBtnVbox.setMinWidth(ouvrageVboxWidth);
        delBtnVbox.setMaxWidth(ouvrageVboxWidth);

        ColumnConstraints colConstraint = new ColumnConstraints(spVbox.getPrefWidth()/ouvrageByRowCount);
        colConstraint.setHalignment(HPos.CENTER);

        List rows = spVbox.getChildren();
        if (rows.size()>0 && ((GridPane) rows.get(rows.size() - 1)).getColumnCount() != ouvrageByRowCount){
            GridPane lastGridPane = (GridPane) rows.get(rows.size() - 1);
            lastGridPane.addColumn(lastGridPane.getColumnCount(), ouvrageVbox);
            lastGridPane.getColumnConstraints().add(colConstraint);
            lastGridPane.setMinWidth((spVbox.getPrefWidth()/ouvrageByRowCount)*(rows.size()+1));
            lastGridPane.setMaxWidth(lastGridPane.getMinWidth());
        } else {
            GridPane gp = new GridPane();
            gp.getColumnConstraints().add(colConstraint);
            gp.setMinWidth(spVbox.getPrefWidth()/ouvrageByRowCount);
            gp.setMaxWidth(gp.getMinWidth());
            gp.addColumn(0, ouvrageVbox);
            spVbox.getChildren().add(gp);
        }
    }

    public void updateOuvrages() {
        homeCtrl.afficheMenu();
    }
}