package Vues;

import Controllers.AjoutOuvrageController;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AjoutOuvrageVue {
    private final HomeVue homeVue;
    private AjoutOuvrageController ajoutOuvrageCtrl;
    @FXML
    TextField tfTitre, tfLieu, tfNbPages, tfAnnee, tfAuteurNom, tfAuteurPrenom, tfAuteurPseudo, tfEditeur, tfImprimeur;
    @FXML
    TextArea taCommentaire;

    public AjoutOuvrageVue(AjoutOuvrageController ajoutOuvrageCtrl, HomeVue homeVue){
        this.ajoutOuvrageCtrl = ajoutOuvrageCtrl;
        this.homeVue = homeVue;
    }

    @FXML
    private void btnValiderClicked(){
        //TODO: Ajouter une vérification des inputs + affichage d'erreurs
        try {
            tfNbPages.setStyle("");
            int numPage = Integer.parseInt(tfNbPages.getText());
            ajoutOuvrageCtrl.ajouterOuvrage(tfTitre.getText(), tfLieu.getText(), numPage,
                    tfAnnee.getText(), taCommentaire.getText(),tfImprimeur.getText(),
                    tfEditeur.getText(), tfAuteurNom.getText(), tfAuteurPrenom.getText(), tfAuteurPseudo.getText());
            ((Stage) tfTitre.getScene().getWindow()).close();
            homeVue.updateOuvrages();
        } catch (NumberFormatException e) {
            tfNbPages.setStyle("-fx-border-color: red");
        }
    }
    
    @FXML
    private void btnAnnulerClicked(){
        ((Stage) tfTitre.getScene().getWindow()).close();
    }
}
