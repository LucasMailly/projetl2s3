package Vues;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import Controllers.AjoutPageController;

public class AjoutPageVue {
	private AjoutPageController ajoutPageCtrl;
    private VisualiserVue visualiserVue;

    public AjoutPageVue(AjoutPageController ajoutPageCtrl, VisualiserVue visualiserVue) {
        this.visualiserVue = visualiserVue;
        this.ajoutPageCtrl = ajoutPageCtrl;
    }

    @FXML
    TextField tfChemin, tfNumPage;

    @FXML
    private void btnValiderClicked() {
        String error = "";
        if (tfNumPage.isDisabled()) {
            String[] paths = tfChemin.getText().split(";");
            for (String p : paths) {
                Pattern pattern = Pattern.compile("^.+\\/\\d+\\.(jpg|png)$");
                if (pattern.matcher(p).find()){
                    String[] pathItems = p.substring(0,p.length()-4).split("/");
                    int numPage = Integer.parseInt(pathItems[pathItems.length-1]);
                    if (numPage >= 1) {
                        error = ajoutPageCtrl.ajouterPages(p, numPage);
                        if (error!="") {
                            System.out.println(error);
                            //TODO: afficher l'erreur dans l'interface
                        }
                    }
                }
            }
        } else {
            try {
                int numPage = Integer.parseInt(tfNumPage.getText());
                if (numPage >= 1) {
                    error = ajoutPageCtrl.ajouterPages(tfChemin.getText(), numPage);
                    if (error!="") {
                        System.out.println(error);
                        //TODO: afficher l'erreur dans l'interface
                    }
                }
            } catch (NumberFormatException e){
                System.out.println(e);
            }
        }
        
        if (error=="") {
            visualiserVue.updatePage();
            ((Stage) tfChemin.getScene().getWindow()).close();
        }
    }
    
    @FXML
    private void btnAnnulerClicked() {
        ((Stage) tfChemin.getScene().getWindow()).close();
    }
    
    @FXML
    private void btnBrowseClicked() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Sélectionner une ou plusieurs pages");
        File UserDesktop = new File(System.getProperty("user.home"));
        if (UserDesktop.exists()) {
            fc.setInitialDirectory(UserDesktop);
        }
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image file(s) jpg/png", "*.jpg","*.png"));
        List<File> files = fc.showOpenMultipleDialog((Stage) tfChemin.getScene().getWindow());
        if (files != null){
            ArrayList<String> pathFiles = new ArrayList<String>();
            for (File file : files) {
                pathFiles.add(file.getPath());
            }
            tfChemin.setText(String.join(";", pathFiles));
            if (pathFiles.size()>1) tfNumPage.setDisable(true);
            else tfNumPage.setDisable(false);
        }
    }
}
