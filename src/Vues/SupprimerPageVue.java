package Vues;

import java.sql.SQLException;

import Controllers.SupprimerPageController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SupprimerPageVue {
	private VisualiserVue visualiserVue;
	private SupprimerPageController supprimerPageCtrl;
	@FXML
    Button btnCancel, btnDelete;
	
	public SupprimerPageVue(VisualiserVue visualiserVue, SupprimerPageController supprimerPageCtrl) {
		this.visualiserVue = visualiserVue;
		this.supprimerPageCtrl = supprimerPageCtrl;
	}
	 
	@FXML
	private void btnDeleteClicked() throws SQLException {
		supprimerPageCtrl.supprimerPage();
		visualiserVue.updateImage();
		((Stage) btnCancel.getScene().getWindow()).close();
	}
	
	@FXML
	private void btnCancelClicked() {
		((Stage) btnCancel.getScene().getWindow()).close();
	}
}
