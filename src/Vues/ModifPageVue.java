package Vues;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.sql.SQLException;

import Controllers.ModifPageController;

/**
 * Classe AjoutPageController
 * @author LAURIN Victor
 */
public class ModifPageVue {
	private ModifPageController modifPageCtrl;
    private VisualiserVue visualiserVue;

    public ModifPageVue(ModifPageController modifPageCtrl, VisualiserVue visualiserVue) {
        this.visualiserVue = visualiserVue;
        this.modifPageCtrl = modifPageCtrl;
    }

    @FXML
    TextField tfChemin;
    
    @FXML
    private void btnValiderClicked() throws SQLException {
    	String error = "";
    	try {
    		error = modifPageCtrl.modifierPage(tfChemin.getText());
    		if (error!="") {
                    System.out.println(error);
                    //TODO: afficher l'erreur dans l'interface
                }
        } catch (NumberFormatException e){
            System.out.println(e);
        }
    	
    	if (error=="") {
            visualiserVue.updatePage();
            ((Stage) tfChemin.getScene().getWindow()).close();
        }
    }
    
    @FXML
    private void btnAnnulerClicked() {
        ((Stage) tfChemin.getScene().getWindow()).close();
    }
    
    @FXML
    private void btnBrowseClicked() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Sélectionner une nouvelle page");
        File UserDesktop = new File(System.getProperty("user.home"));
        if (UserDesktop.exists()) {
            fc.setInitialDirectory(UserDesktop);
        }
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image file(s) jpg/png", "*.jpg","*.png"));
        File file = fc.showOpenDialog((Stage) tfChemin.getScene().getWindow());
        if (file != null){
            String pathFile = file.getPath();
            tfChemin.setText(String.join(";", pathFile));
        }
    }
}
