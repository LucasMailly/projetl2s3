package Vues;

import java.io.IOException;
import java.sql.SQLException;

import Controllers.AjoutPageController;
import Controllers.ImgViewerController;
import Controllers.ModifPageController;
import Controllers.SupprimerPageController;
import Controllers.VisualiserController;
import Modeles.Composante;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Modality;

/**
 * Classe VisualiserVue
 * @author MAILLY Lucas
 */
public class VisualiserVue {
	private VisualiserController visualiserCtrl;
    private ImgViewerController imgViewerCtrl;
    private ImgViewerVue vueOriginal, vueAnalyse;
    private Canvas canvasOriginal, canvasAnalyse;
    private Composante selectedComposante;

    public VisualiserVue(VisualiserController visualiserCtrl, ImgViewerController imgViewerCtrl) {
        this.imgViewerCtrl = imgViewerCtrl;
        this.visualiserCtrl = visualiserCtrl;
    }

    @FXML
    Button btnModifDataOuvrage, btnValidDataOuvrage, btnModifDataCC, btnValidDataCC, btnHome, btnAddPage, btnDeletePage , btnModifPage, btnPreviousPage, btnNextPage;
    @FXML
    TextField tfTitre, tfAuteurNom, tfAuteurPr�nom, tfAuteurPseudo, tfImp, tfEditeur, tfAnnee, tfNbPages, tfLieu, tfTranscription, tfType, tfPolice, tfLargeur, tfHauteur, tfXStart, tfYStart;
    @FXML
    Label lblTitre, lblAuteur, lblImp, lblEditeur, lblAnnee, lblNbPages, lblLieu, lblTranscription, lblType, lblPolice, lblLargeur, lblHauteur, lblNumPage, lblXStart, lblYStart;
    @FXML
    Text textCommOuvrage, textCommCC, textKeywords;
    @FXML
    TextArea textAreaCommOuvrage, textAreaCommCC, textAreaKeywords;
    @FXML
    AnchorPane originalAnchorPane, analyseAnchorPane;
    @FXML
    Tab tabOriginal, tabAnalyse;
    @FXML
    Slider sliderPage;
    @FXML
    VBox vboxInfosCC;
    @FXML
    ScrollPane spInfosCC;

    /**
     * Remplace la sc�ne courante par la sc�ne home (sc�ne initiale)
     * @throws IOException
     */
    @FXML
    private void returnHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("home.fxml"));
        Stage window = (Stage) btnHome.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * M�thode appel�e automatiquement � l'ouverture de la fen�tre
     * Initialise les deux canvas (analyse et originale)
     * @throws SQLException 
     */
    @FXML
    private void initialize() {
        hideDataOuvrageInput();
        hideDataCCInput();
        hide(vboxInfosCC);
        spInfosCC.setDisable(true);
        btnModifDataCC.setDisable(true);
        vueOriginal = new ImgViewerVue((int) originalAnchorPane.getPrefWidth(),(int) originalAnchorPane.getPrefHeight(), false, imgViewerCtrl, this);
        vueAnalyse = new ImgViewerVue((int) analyseAnchorPane.getPrefWidth(),(int) analyseAnchorPane.getPrefHeight(), true, imgViewerCtrl, this);
        canvasOriginal = vueOriginal.getCanvas();
        canvasAnalyse = vueAnalyse.getCanvas();
        originalAnchorPane.getChildren().add(canvasOriginal);
        analyseAnchorPane.getChildren().add(canvasAnalyse);
        visualiserCtrl.initialiseOuvrage();
        visualiserCtrl.initialisePage();
    }

    @FXML
    private void previousPageClicked(){
        visualiserCtrl.previousPage();
        imgViewerCtrl.changePage(visualiserCtrl.getPage());
        setComposante(null);
        updateImage();
    }

    @FXML
    private void nextPageClicked(){
        visualiserCtrl.nextPage();
        imgViewerCtrl.changePage(visualiserCtrl.getPage());
        setComposante(null);
        updateImage();
    }

    /**
     * Synchronise l'origine et les �chelles des images des deux canvas quand on change de tab
     */
    @FXML
    private void tabChanged(){
        if (vueOriginal != null && vueAnalyse != null) {
            if (tabOriginal.isSelected()){
                vueOriginal.setOrigin(vueAnalyse.getOrigin());
                vueOriginal.setSize(vueAnalyse.getSize());
            } else {
                vueAnalyse.setOrigin(vueOriginal.getOrigin());
                vueAnalyse.setSize(vueOriginal.getSize());
                setComposante(null);
            }
        }
    }

    /**
     * Switch les donn�es de l'ouvrage en mode �ditable ou lecture
     */
    @FXML
    private void btnDataOuvrageClicked() {
        if (!tfAuteurNom.isVisible()) {
            hideDataOuvrageOutput();
        } else {
            lblTitre.setText(tfTitre.getText());
            lblAuteur.setText(tfAuteurNom.getText() + " " + tfAuteurPr�nom.getText() + " (" + tfAuteurPseudo.getText() + ")");
            lblImp.setText(tfImp.getText());
            lblEditeur.setText(tfEditeur.getText());
            lblAnnee.setText(tfAnnee.getText());
            lblNbPages.setText(Integer.toString(Integer.parseInt(tfNbPages.getText())));
            lblLieu.setText(tfLieu.getText());
            textCommOuvrage.setText(textAreaCommOuvrage.getText());
            visualiserCtrl.modifierOuvrage(tfTitre.getText(), tfAuteurNom.getText(), tfAuteurPr�nom.getText(),
                    tfAuteurPseudo.getText(), lblImp.getText(), lblEditeur.getText(),
                    lblAnnee.getText(), Integer.parseInt(lblNbPages.getText()), lblLieu.getText(), textCommOuvrage.getText());
            hideDataOuvrageInput();
        }
    }
    private void hideDataOuvrageOutput(){
        hide(lblTitre);hide(lblAuteur);hide(lblImp);hide(lblEditeur);hide(lblAnnee);hide(lblNbPages);hide(lblLieu);
        hide(textCommOuvrage);
        hide(btnModifDataOuvrage);
        show(tfTitre);show(tfAuteurNom);show(tfAuteurPr�nom);show(tfAuteurPseudo);show(tfImp);show(tfEditeur);show(tfAnnee);show(tfNbPages);show(tfLieu);
        show(textAreaCommOuvrage);
        show(btnValidDataOuvrage);
    }
    private void hideDataOuvrageInput(){
        show(lblTitre);show(lblAuteur);show(lblImp);show(lblEditeur);show(lblAnnee);show(lblNbPages);show(lblLieu);
        show(textCommOuvrage);
        show(btnModifDataOuvrage);
        hide(tfTitre);hide(tfAuteurNom);hide(tfAuteurPr�nom);hide(tfAuteurPseudo);hide(tfImp);hide(tfEditeur);hide(tfAnnee);hide(tfNbPages);hide(tfLieu);
        hide(textAreaCommOuvrage);
        hide(btnValidDataOuvrage);
    }

    /**
     * Switch les donn�es de la page en mode �ditable ou lecture
     */
    @FXML
    private void btnDataCCClicked() {
        if (!tfTranscription.isVisible()) {
            hideDataCCOutput();
        } else {
            lblTranscription.setText(tfTranscription.getText());
            lblType.setText(tfType.getText());
            lblPolice.setText(tfPolice.getText());
            lblLargeur.setText(tfLargeur.getText());
            lblHauteur.setText(tfHauteur.getText());
            lblXStart.setText(tfXStart.getText());
            lblYStart.setText(tfYStart.getText());
            textCommCC.setText(textAreaCommCC.getText());
            textKeywords.setText(textAreaKeywords.getText());
            int largeur = selectedComposante.getWidth();
            int hauteur = selectedComposante.getHeight();
            int xStart = selectedComposante.getxStart();
            int yStart = selectedComposante.getyStart();
            double imgWidth = vueAnalyse.getSize()[0];
            double imgHeight = vueAnalyse.getSize()[1];
            try{
                int tmpLargeur = Integer.valueOf(lblLargeur.getText());
                if (tmpLargeur>0 && tmpLargeur-selectedComposante.getxStart() <= imgWidth)
                    largeur = tmpLargeur;
            }catch (NumberFormatException e){
                tfLargeur.setText(String.valueOf(largeur));
                lblLargeur.setText(String.valueOf(largeur));
            }
            try{
                int tmpHauteur = Integer.valueOf(lblHauteur.getText());
                if (tmpHauteur>0 && tmpHauteur-selectedComposante.getyStart() <= imgHeight)
                    hauteur = tmpHauteur;
            }catch (NumberFormatException e){
                tfHauteur.setText(String.valueOf(hauteur));
                lblHauteur.setText(String.valueOf(hauteur));
            }
            try{
                int tmpXStart = Integer.valueOf(lblXStart.getText());
                if (tmpXStart>=0 && tmpXStart<=imgWidth)
                    xStart = tmpXStart;
            }catch (NumberFormatException e){
                tfXStart.setText(String.valueOf(xStart));
                lblXStart.setText(String.valueOf(xStart));
            }
            try{
                int tmpYStart = Integer.valueOf(lblYStart.getText());
                if (tmpYStart>=0 && tmpYStart<=imgHeight)
                    yStart = tmpYStart;
            }catch (NumberFormatException e){
                tfYStart.setText(String.valueOf(yStart));
                lblYStart.setText(String.valueOf(yStart));
            }
            visualiserCtrl.modifierCC(selectedComposante,lblTranscription.getText(),lblType.getText(),
                    lblPolice.getText(),largeur,hauteur,xStart,yStart,textCommCC.getText(),
                    textKeywords.getText());
            hideDataCCInput();
        }
    }
    private void hideDataCCOutput(){
        hide(lblTranscription);hide(lblType);hide(lblPolice);hide(lblLargeur);hide(lblHauteur);hide(lblXStart);hide(lblYStart);
        hide(textCommCC);
        hide(textKeywords);
        hide(btnModifDataCC);
        show(tfTranscription);show(tfType);show(tfPolice);show(tfLargeur);show(tfHauteur);show(tfXStart);show(tfYStart);
        show(textAreaCommCC);
        show(textAreaKeywords);
        show(btnValidDataCC);
    }
    private void hideDataCCInput(){
        show(lblTranscription);show(lblType);show(lblPolice);show(lblLargeur);show(lblHauteur);show(lblXStart);show(lblYStart);
        show(textCommCC);
        show(textKeywords);
        show(btnModifDataCC);
        hide(tfTranscription);hide(tfType);hide(tfPolice);hide(tfLargeur);hide(tfHauteur);hide(tfXStart);hide(tfYStart);
        hide(textAreaCommCC);
        hide(textAreaKeywords);
        hide(btnValidDataCC);
    }
    
   @FXML
   private void btnAddPageClicked() {
        AjoutPageController ajoutPageController = new AjoutPageController(visualiserCtrl.getIdOuvrage(),this);
        AjoutPageVue ajoutPageVue = ajoutPageController.getAjoutPageVue();
        FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("ajoutPage.fxml"));
        fxmlLoader.setController(ajoutPageVue);
        try {
            Stage stage = new Stage();
            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Ajout de pages");
            stage.getIcons().add(new Image("file:../../resources/icon.png"));
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
           ex.printStackTrace();
        }
    }

   @FXML
   private void btnModifPageClicked() {
	   ModifPageController modifPageController = new ModifPageController(visualiserCtrl.getIdOuvrage(), this, visualiserCtrl.getNumPage());
       ModifPageVue modifPageVue = modifPageController.getModifPageVue();
       FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("modifPage.fxml"));
       fxmlLoader.setController(modifPageVue);
       try {
           Stage stage = new Stage();
           Scene scene = new Scene(fxmlLoader.load());
           stage.setTitle("Modification d'une page");
           stage.getIcons().add(new Image("file:../../resources/icon.png"));
           stage.setScene(scene);
           stage.initModality(Modality.APPLICATION_MODAL);
           stage.show();
       } catch (IOException ex) {
          ex.printStackTrace();
       }
   }

   @FXML
   private void btnDeletePageClicked() {
	   SupprimerPageController supprimerPageCtrl = new SupprimerPageController(visualiserCtrl.getIdOuvrage(), visualiserCtrl.getNumPage() ,this);
	   SupprimerPageVue supprimerPageVue = new SupprimerPageVue(this, supprimerPageCtrl);
	   FXMLLoader fxmlLoader = new FXMLLoader(AppVue.class.getResource("supprimerPage.fxml"));
       fxmlLoader.setController(supprimerPageVue);
		try {
			Stage stage = new Stage();
           Scene scene = new Scene(fxmlLoader.load());
           stage.setTitle("Suppression d'un ouvrage");
           stage.getIcons().add(new Image("file:../../resources/icon.png"));
           stage.setScene(scene);
           stage.initModality(Modality.APPLICATION_MODAL);
           stage.show();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
   }

    /**
     * Cache l'�l�ment pass� en param�tre et adapte son �l�ment parent pour faire comme s'il n'�tait pas compris dedans
     * @param element
     */
    private void hide(Node element) {
        element.setVisible(false);
        element.setManaged(false);
    }

    /**
     * Affiche l'�l�ment pass� en param�tre s'il �tait cach�
     * @param element
     */
    private void show(Node element) {
        element.setVisible(true);
        element.setManaged(true);
    }
    
    public void ajouterInfosOuvrage(String lieux, String titre,
    		int nbPages, String annee, String commentaire, String imp, String edit, 
    		String auteurNom, String auteurPr�nom, String auteurPseudo) {
        tfTitre.setText(titre);
    	tfAuteurNom.setText(auteurNom);
    	tfAuteurPr�nom.setText(auteurPr�nom);
    	tfAuteurPseudo.setText(auteurPseudo);
    	tfImp.setText(imp);
    	tfEditeur.setText(edit);
    	tfAnnee.setText(annee);
    	tfNbPages.setText(Integer.toString(nbPages));
    	tfLieu.setText(lieux);
    	textCommOuvrage.setText(commentaire);
        lblTitre.setText(titre);
    	lblAuteur.setText(auteurNom + " " + auteurPr�nom + " (" + auteurPseudo + ")");
    	lblImp.setText(imp);
    	lblEditeur.setText(edit);
    	lblAnnee.setText(annee);
    	lblNbPages.setText(Integer.toString(nbPages));
    	lblLieu.setText(lieux);
    	textAreaCommOuvrage.setText(commentaire);
    }

    public void clearImage() {
        vueOriginal.reset();
        vueAnalyse.reset();
    }

    public void updateImage() {
        vueOriginal.updateImage();
        vueAnalyse.updateImage();
    }

    public void changeImage(String pathImage){
        imgViewerCtrl.setImagePath(pathImage);
        updateImage();
    }

    public void updatePage(){
        imgViewerCtrl.changePage(visualiserCtrl.getPage());
        visualiserCtrl.updatePage();
    }

    public void setNumPage(int numPage) {
        lblNumPage.setText(String.valueOf(numPage));
    }

    public void setComposante(Composante cc){
        this.selectedComposante=cc;
        vueAnalyse.setClickedCC(cc);
        afficheInfosCC();
    }

    public void afficheInfosCC() {
        if (selectedComposante != null) {
            show(vboxInfosCC);
            spInfosCC.setDisable(false);
            btnModifDataCC.setDisable(false);
            lblLargeur.setText(String.valueOf(selectedComposante.getWidth()));
            lblHauteur.setText(String.valueOf(selectedComposante.getHeight()));
            lblXStart.setText(String.valueOf(selectedComposante.getxStart()));
            lblYStart.setText(String.valueOf(selectedComposante.getyStart()));
            textCommCC.setText(selectedComposante.getCommentaire());
            lblTranscription.setText(selectedComposante.getTranscription());
            lblPolice.setText(selectedComposante.getPolice());
            lblType.setText(selectedComposante.getType());
            textKeywords.setText(selectedComposante.getKeywords());
            tfLargeur.setText(String.valueOf(selectedComposante.getWidth()));
            tfHauteur.setText(String.valueOf(selectedComposante.getHeight()));
            tfXStart.setText(String.valueOf(selectedComposante.getxStart()));
            tfYStart.setText(String.valueOf(selectedComposante.getyStart()));
            textAreaCommCC.setText(selectedComposante.getCommentaire());
            tfTranscription.setText(selectedComposante.getTranscription());
            tfPolice.setText(selectedComposante.getPolice());
            tfType.setText(selectedComposante.getType());
            textAreaKeywords.setText(selectedComposante.getKeywords());
        } else {
            if (tfTranscription.isVisible()) {
                hideDataCCInput();
            }

            hide(vboxInfosCC);
            spInfosCC.setDisable(true);
            btnModifDataCC.setDisable(true);
        }
    }
}