module App {
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires java.desktop;
    requires java.scripting;
    requires java.compiler;
    requires java.rmi;
    requires java.sql;
	requires junit;
	
	opens Vues to javafx.graphics, javafx.fxml;
	
	exports test to junit;
}
