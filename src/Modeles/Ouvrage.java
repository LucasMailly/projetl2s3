package Modeles;

/**
 * Permet de cr�er un ouvrage
 * @author LAURIN Victor
 * 
 */
public class Ouvrage {
	//Attributs
	private int idOuvrage;
	private String lieux;
	private String titre;
	private int nbPages;
	private String annee;
	private String commentaire;
	private String imprimeur;
	private String editeur;
	private String auteurNom;
	private String auteurPrenom;
	private String auteurPseudo;
	private static int auto_increment = 0;
	
	//Constructeurs
	/**
	 * Constructeur de la classe Ouvrage, permettant de l'instanci�e sans param�tre,
	 * incr�mentant l'ID automatiquement
	 */
	public Ouvrage() {
		idOuvrage = auto_increment;
		auto_increment++;
	}
	
	/**
	 * Constructeur de la classe Ouvrage, permettant de l'instanci�e
	 * avec son nombre de pages et incr�mentant l'ID automatiquement
	 * @param nbPages Nombre de pages que contient l'ouvrage
	 */
	public Ouvrage(int nbPages) {
		idOuvrage = auto_increment;
		auto_increment++;
		this.nbPages = nbPages;
	}
	
	/**
	 * Constructeur de la classe Ouvrage, permettant de l'instanci�e
	 * avec son nombre de pages et son ID
	 * @param idOuvrage ID de l'ouvrage
	 * @param nbPages Nombre de pages que contient l'ouvrage
	 */
	public Ouvrage(int idOuvrage, int nbPages) {
		this.idOuvrage = idOuvrage;
		this.nbPages = nbPages;
	}
	
	/**
	 * Constructeur de la classe Ouvrage, permettant de l'instanci�e
	 * avec son lieu de provenance, son titre, son nombre de pages, son ann�e de r�daction, 
	 * des commenatires suppl�mantaires sur le livre, ainsi que l'ID de son imprimeur, 
	 * son �diteur et son auteur, avec l'ID de l'ouvrage incr�ment� automatiquement
	 * @param lieux Lieu de provenance de l'ouvrage
	 * @param titre Titre de l'ouvrage
	 * @param nbPages Nombre de page que contient l'ouvrage
	 * @param annee Ann�e de r�daction de l'ouvrage
	 * @param commentaire Commentaires � apporter sur l'ouvrage
	 * @param imprimeur Nom de l'imprimeur de l'ouvrage
	 * @param editeur Nom de l'�diteur de l'ouvrage
	 * @param auteurNom Nom de l'auteur de l'ouvrage
	 * @param auteurPrenom Pr�nom de l'auteur de l'ouvrage
	 * @param auteurPseudo Pseudo de l'auteur de l'ouvrage
	 */
	public Ouvrage(String lieux, String titre, int nbPages, String annee, String commentaire, String imprimeur, String editeur,
				   String auteurNom, String auteurPrenom, String auteurPseudo) {
		idOuvrage = auto_increment;
		auto_increment++;
		this.lieux = lieux;
		this.titre = titre;
		this.nbPages = nbPages;
		this.annee = annee;
		this.commentaire = commentaire;
		this.imprimeur = imprimeur;
		this.editeur = editeur;
		this.auteurNom = auteurNom;
		this.auteurPrenom = auteurPrenom;
		this.auteurPseudo = auteurPseudo;
	}
	
	/**
	 * Constructeur de la classe Ouvrage, permettant de l'instanci�e
	 * avec son lieu de provenance, son titre, son nombre de pages, son ann�e de r�daction, 
	 * des commentaires suppl�mantaires sur le livre, ainsi que son ID
	 * et ceux de son imprimeur, son �diteur et son auteur
	 * @param idOuvrage ID de l'ouvrage
	 * @param lieux Lieu de provenance de l'ouvrage
	 * @param titre Titre de l'ouvrage
	 * @param nbPages Nombre de page que contient l'ouvrage
	 * @param annee Ann�e de r�daction de l'ouvrage
	 * @param commentaire Commentaires � apporter sur l'ouvrage
	 * @param imprimeur Nom de l'imprimeur de l'ouvrage
	 * @param editeur Nom de l'�diteur de l'ouvrage
	 * @param auteurNom Nom de l'auteur de l'ouvrage
	 * @param auteurPrenom Pr�nom de l'auteur de l'ouvrage
	 * @param auteurPseudo Pseudo de l'auteur de l'ouvrage
	 */
	public Ouvrage(int idOuvrage, String lieux, String titre, int nbPages, String annee, String commentaire, String imprimeur, String editeur,
				   String auteurNom, String auteurPrenom, String auteurPseudo) {
		this.idOuvrage = idOuvrage;
		this.lieux = lieux;
		this.titre = titre;
		this.nbPages = nbPages;
		this.annee = annee;
		this.commentaire = commentaire;
		this.imprimeur = imprimeur;
		this.editeur = editeur;
		this.auteurNom = auteurNom;
		this.auteurPrenom = auteurPrenom;
		this.auteurPseudo = auteurPseudo;
	}
	
	//M�thodes
	/**
	 * Retourne l'ID de l'ouvrage
	 * @return L'attribut idOuvrage
	 */
	public int getIdOuvrage() {
		return idOuvrage;
	}

	/**
	 * Retourne le lieu de provenance de l'ouvrage
	 * @return L'attribut lieux
	 */
	public String getLieux() {
		return lieux;
	}
	
	/**
	 * Retourne le titre de l'ouvrage
	 * @return L'attribut titre
	 */
	public String getTitre() {
		return titre;
	}
	
	/**
	 * Retourne le nombre de pages contenues de l'ouvrage
	 * @return L'attribut nbPages
	 */
	public int getNbPages() {
		return nbPages;
	}
	
	/**
	 * Retourne l'ann�e de r�daction de l'ouvrage
	 * @return L'attribut annee
	 */
	public String getAnnee() {
		return annee;
	}
	
	/**
	 * Retourne les commentaires r�dig�s sur l'ouvrage
	 * @return L'attribut commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}
	
	/**
	 * Retourne le nom de l'imprimeur de l'ouvrage
	 * @return L'attribut imprimeur
	 */
	public String getImprimeur() {
		return imprimeur;
	}
	
	/**
	 * Retourne le nom de l'�diteur de l'ouvrage
	 * @return L'attribut editeur
	 */
	public String getEditeur() {
		return editeur;
	}
	
	/**
	 * Retourne le nom de l'auteur de l'ouvrage
	 * @return L'attribut auteur
	 */
	public String getAuteurNom() {
		return auteurNom;
	}
	
	/**
	 * Retourne le pr�nom l'auteur de l'ouvrage
	 * @return L'attribut auteur
	 */
	public String getAuteurPrenom() {
		return auteurPrenom;
	}
	
	/**
	 * Retourne le pseudonyme de l'auteur de l'ouvrage
	 * @return L'attribut auteur
	 */
	public String getAuteurPseudo() {
		return auteurPseudo;
	}
}
