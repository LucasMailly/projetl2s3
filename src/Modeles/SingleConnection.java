package Modeles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Permet de cr�er une connexion unique � la base de donn�es
 * @author LAURIN Victor
 * 
 */
public class SingleConnection {
	//Attributs
	private static Connection conn;
	
	//Constructeur
	/**
	 * Constructeur de la classe SingleConnection permettant de l'instanci�e avec l'URL d'une BD,
	 * ainsi que l'identifiant et le mot de passe pour s'y connecter
	 * @param url
	 * @param login
	 * @param mdp
	 * @see System#out
	 * @throws ClassNotFoundException Si une des classes qui doit �tre g�n�r�e ne peux pas l'�tre
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	private SingleConnection(String url, String login, String mdp) throws ClassNotFoundException, SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("Driver OK !");
			this.conn = DriverManager.getConnection(url, login, mdp);
			System.out.println("Connexion effective !");
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	//Méthodes
	/**
	 * Permet de r�cup�rer une connection � la base de donn�es
	 * @param url Adresse de connexion � la BD
	 * @param login Nom d'utilisateur pour se connecter � la BD
	 * @param mdp Mot de passe n�cessaire � la connexion � la BD
	 * @return L'attribut conn d'un objet SingleConnection tout juste cr��
	 * @throws ClassNotFoundException Si une des classes qui doit �tre g�n�r�e ne peux pas l'�tre
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	 public static Connection getInstance(String url, String login, String mdp) throws ClassNotFoundException, SQLException {
		 if(conn == null){  new SingleConnection(url, login, mdp);}
		 return conn;
	 }
}
