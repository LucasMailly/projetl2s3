package Modeles;

/**
 * Classe Composante
 * @author MAILLY Lucas
 */
public class Composante {
    //Attributs
    private final int idComposante;
    private final int xStart;
    private final int yStart;
    private final int width;
    private final int height;
    private final String commentaire;
    private final int idPage;
    private final String police;
    private final String type;
    private final String transcription;
    private final String keywords;

    //Constructeurs
    /**
     * Constructeur de Composante initialisé avec tout ses attributs
     * @param idComposante Id de la composante
     * @param xStart coordonnée X de la composante
     * @param yStart coordonnée Y de la composante
     * @param width Largeur de la composante
     * @param height Longueur de la composante
     * @param commentaire Commentaire de la composante
     * @param idPage Id de la page de la composante
     * @param police police de la composante
     * @param type type de la composante
     * @param transcription transcription de la composante
     * @param keywords mots-clés associés à la composante
     */
    public Composante(int idComposante, int xStart, int yStart, int width, int height, String commentaire, int idPage, String police, String type, String transcription, String keywords) {
        this.idComposante = idComposante;
        this.xStart = xStart;
        this.yStart = yStart;
        this.width = width;
        this.height = height;
        this.commentaire = commentaire;
        this.idPage = idPage;
        this.police = police;
        this.type = type;
        this.transcription = transcription;
        this.keywords = keywords;
    }

    //Méthodes

    /**
     * @return L'id de la composante
     */
    public int getIdComposante() {
        return idComposante;
    }

    /**
     * @return La coordonnée X de la composante
     */
    public int getxStart() {
        return xStart;
    }

    /**
     * @return La coordonnée Y de la composante
     */
    public int getyStart() {
        return yStart;
    }

    /**
     * @return La largeur de la composante
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return La hauteur de la composante
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return Le commentaire de la composante
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @return L'id de la page de la composante
     */
    public int getIdPage() {
        return idPage;
    }

    /**
     * @return La police de la composante
     */
    public String getPolice() {
        return police;
    }

    /**
     * @return Le type de la composante
     */
    public String getType() {
        return type;
    }

    /**
     * @return La transcription de la composante
     */
    public String getTranscription() {
        return transcription;
    }

    /**
     * @return Les mots-clés associés à la composante
     */
    public String getKeywords() {
        return keywords;
    }

}