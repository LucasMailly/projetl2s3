package Modeles;

import analyzer.app.CCLabeler;
import analyzer.app.MeasuresList;

import java.util.ArrayList;

/**
 * Permet de gérer les différentes fonctionnalités lorsqu'une page est affichée
 * @author MAILLY Lucas
 *
 */
public class ImgViewer {

	private ArrayList<Composante> composantes = new ArrayList<>();
	private Page page;

	private GestionComposante dao = new GestionComposante(
			"jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
			"root",
			"");

	/**
	 * Permet de trouver le nombre de caractères présents sur une page
	 * (Traite l'image et compte les particules + récupère les mesures de l'image traitée)
	 * @see CCLabeler#process(String)
	 * @see CCLabeler#getMeasures()
	 * @return Les mesures de l'image traitée
	 */
    public ArrayList<Composante> getComposantes() {
		return composantes;
    }

	public void analysePage(){
		if (page != null){
			CCLabeler counter = new CCLabeler();
			counter.process(page.getCheminImage());
			MeasuresList measures = counter.getMeasures();
			if (dao.setMeasures(measures, page))
				dao.setPageAnalysed(page);
				page.setAnalyse(true);
			composantes = dao.getComposantes(page);
		}else{
			composantes = new ArrayList<>();
		}
	}

	public void changePage(Page page){
		this.page = page;
		if (page != null){
			if (page.isAnalyse()) {
				composantes = dao.getComposantes(page);
			} else{
				analysePage();
			}
		}else{
			if (page != null) {
				composantes = new ArrayList<>();
			}
		}
	}
}
