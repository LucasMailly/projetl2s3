package Modeles;

/**
 * Permet de cr�er une page
 * @author LAURIN Victor
 * 
 */
public class Page {
	//Attributs
	private int idPage;
	private String cheminImage;
	private int numeroPage;
	private boolean analyse;
	private int idOuvrage;
	private static int auto_increment = 0;
	
	//Constructeur
	/**
	 * Constructeur de la classe Page, permettant de l'instanci�e avec un ID, le chemin de l'image, 
	 * un num�ro de page et un ID d'ouvrage
	 * @param idPage ID de la page
	 * @param cheminImage Chemin de l'image
	 * @param numeroPage Num�ro de la page
	 * @param idOuvrage ID de l'ouvrage dans lequel est contenu la paage
	 */
	public Page(int idPage, String cheminImage, int numeroPage, int idOuvrage) {
		this.idPage = idPage;
		this.cheminImage = cheminImage;
		this.numeroPage = numeroPage;
		this.idOuvrage = idOuvrage;
		analyse = false;
	}
	
	/**
	 * Constructeur de la classe Page, permettant de l'instanci�e avec le chemin de l'image, 
	 * un num�ro de page et un ID d'ouvrage et incr�mentant l'ID de la page automatiquement
	 * @param cheminImage Chemin de l'image
	 * @param numeroPage Num�ro de la page
	 * @param idOuvrage ID de l'ouvrage dans lequel est contenu la page
	 */
	public Page(String cheminImage, int numeroPage, int idOuvrage) {
		idPage = auto_increment;
		auto_increment++;
		this.cheminImage = cheminImage;
		this.numeroPage = numeroPage;
		this.idOuvrage = idOuvrage;
		analyse = false;
	}
	
	/**
	 * Constructeur de la classe Page, permettant de l'instanci�e avec un ID, le chemin de l'image, 
	 * l'analyse de l'image, un num�ro de page et un ID d'ouvrage
	 * @param idPage ID de la page
	 * @param cheminImage Chemin de l'image
	 * @param numeroPage Num�ro de la page
	 * @param analyse Bool�en permettant de savoir si l'image a �t� analys�e
	 * @param idOuvrage ID de l'ouvrage dans lequel est contenu la page
	 */
	public Page(int idPage, String cheminImage, int numeroPage, boolean analyse, int idOuvrage) {
		this.idPage = idPage;
		this.cheminImage = cheminImage;
		this.numeroPage = numeroPage;
		this.analyse = analyse;
		this.idOuvrage = idOuvrage;
	}
	
	//M�thodes
	/**
	 * Retourne l'ID de la page
	 * @return L'attribut idPage
	 */
	public int getIdPage() {
		return idPage;
	}
	
	/**
	 * Retourne le chemin de l'image
	 * @return L'attribut cheminImage
	 */
	public String getCheminImage() {
		return cheminImage;
	}
	
	/**
	 * Retourne le num�ro de la page
	 * @return L'attribut numeroPage
	 */
	public int getNumeroPage() {
		return numeroPage;
	}
	
	/**
	 * Retourne le r�sultat de l'analyse de l'image
	 * @return L'attribut analyse
	 */
	public boolean isAnalyse() {
		return analyse;
	}
	
	/**
	 * Retourne l'ID de l'ouvrage contenant la page
	 * @return L'attribut idOuvrage
	 */
	public int getIdOuvrage() {
		return idOuvrage;
	}

	public void setAnalyse(boolean analyse) {
		this.analyse=analyse;
	}
}

