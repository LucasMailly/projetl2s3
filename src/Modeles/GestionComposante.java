package Modeles;

import analyzer.app.Measure;
import analyzer.app.MeasuresList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestionComposante {

    private String url;
    private String login;
    private String password;
    private Connection con;

    public GestionComposante(String url, String login, String password){
        this.url = url;
        this.login = login;
        this.password = password;
        try {
            this.con = SingleConnection.getInstance(url, login, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean setMeasures(MeasuresList measures, Page page){
        String sql = "INSERT INTO Composante (xStart, yStart, width, height, idPage) VALUES (?,?,?,?,?)";
        boolean success = true;
        int idPage = page.getIdPage();
        try {
            for (Measure m : measures){
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setInt(1, m.getXstart());
                pstmt.setInt(2,m.getYstart());
                pstmt.setInt(3,m.getWidth());
                pstmt.setInt(4,m.getHeight());
                pstmt.setInt(5,idPage);
                pstmt.execute();
                pstmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            success = false;
        }
        return success;

    }

    public ArrayList<Composante> getComposantes(Page page) {
        String sql = "SELECT *," +
                "(SELECT GROUP_CONCAT(mot SEPARATOR '\n') FROM Caracteriser GROUP BY idComposante HAVING idComposante=C.idComposante) AS keywords " +
                "FROM Composante C WHERE idPage=?";
        ArrayList<Composante> composantes = new ArrayList<>();
        try{
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, page.getIdPage());
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                Composante c = new Composante(
                        res.getInt("idComposante"),
                        res.getInt("xStart"),
                        res.getInt("yStart"),
                        res.getInt("width"),
                        res.getInt("height"),
                        res.getString("commentaire"),
                        res.getInt("idPage"),
                        res.getString("nomPolice"),
                        res.getString("nomComposante"),
                        res.getString("transcription"),
                        res.getString("keywords")
                );
                composantes.add(c);
            }
            pstmt.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return composantes;
    }

    public void setPageAnalysed(Page page){
        String sql = "UPDATE Page SET analyse=true WHERE idPage=?";
        try {
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, page.getIdPage());
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modifierCC(int idComposante, String transcription, String type, String police,int largeur,int hauteur,int xStart,int yStart,String commCC) {
        String sql = "UPDATE Composante SET transcription=?,nomComposante=?,nomPolice=?,width=?,height=?,xStart=?,yStart=?,commentaire=? WHERE idComposante=?";
        try {
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1,transcription);
            pstmt.setString(2,type);
            pstmt.setString(3,police);
            pstmt.setInt(4,largeur);
            pstmt.setInt(5,hauteur);
            pstmt.setInt(6,xStart);
            pstmt.setInt(7,yStart);
            pstmt.setString(8,commCC);
            pstmt.setInt(9,idComposante);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
        }
    }

    public void createNomComposante(String type) {
        try {
            String sql = "INSERT INTO TypeComposante VALUES (?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, type);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
        }

    }

    public void createTranscription(String transcription) {
        try {
            String sql = "INSERT INTO Transcription VALUES (?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, transcription);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
        }
    }

    public void createNomPolice(String police) {
        try {
            String sql = "INSERT INTO Police VALUES (?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, police);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
        }
    }

    public void createKeyword(String kw){
        try {
            String sql = "INSERT INTO motsCle VALUES (?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, kw);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
        }
    }

    public void removeKeyword(int idComposante, String kw){
        try {
            String sql = "DELETE FROM Caracteriser WHERE idComposante=? AND mot=?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1,idComposante);
            pstmt.setString(2,kw);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addKeyword(int idComposante, String kw) {
        try {
            String sql = "INSERT INTO Caracteriser VALUES (?,?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, kw);
            pstmt.setInt(2,idComposante);
            pstmt.execute();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
