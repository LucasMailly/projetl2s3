package Modeles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Permet de g�rer un ouvrage et les pages qu'il contient
 * @author LAURIN Victor
 * 
 */
public class GestionOuvrage {
	//Attributs
	private String url;
	private String login;
	private String password;
	private Connection con;
	
	//Constructeurs
	/**
	 * Constructeur de GestionOuvrage, permettant de l'instanci�e avec des informations de connexion � une BD
	 * @param url Adresse de connexion � la BD
	 * @param login Nom d'utilisateur pour se connecter � la BD
	 * @param password Mot de passe n�cessaire � la connexion � la BD
	 * @see SingleConnection#getInstance(String, String, String)
	 * @throws ClassNotFoundException Si une des classes qui doit �tre g�n�r� ne peux pas l'�tre
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public GestionOuvrage(String url, String login, String password) {
		this.url = url;
		this.login = login;
		this.password = password;
		try {
			this.con = SingleConnection.getInstance(url, login, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//M�thodes
	/**
	 * Renvoie la liste de tous les ouvrages contenus dans la BD
	 * @see Connection#createStatement()
	 * @see Statement#executeQuery(String)
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(String)
	 * @see ResultSet#getString(String)
	 * @see ArrayList#add(Object)
	 * @see Statement#close()
	 * @return Liste des ouvrages contenus dans la base de donn�es
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public List<Ouvrage> afficherMenu() {
		ArrayList<Ouvrage> ouvrages = new ArrayList<Ouvrage>();
		try {
			Statement stmt = con.createStatement();
			ResultSet res = stmt.executeQuery(
					"SELECT idOuvrage,titre,lieux,nbPage,annee,commentaire," +
							"(SELECT I.nom FROM Imprimeur I WHERE I.idImprimeur=O.idImprimeur) AS imprimeur, " +
							"(SELECT E.nom FROM Editeur E WHERE E.idEditeur=O.idEditeur) AS editeur, " +
							"(SELECT A.nom FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurNom, " +
							"(SELECT A.prenom FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurPr�nom, " +
							"(SELECT A.pseudonyme FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurPseudo " +
							"FROM Ouvrage O ORDER BY titre");
			while (res.next()){
				Ouvrage ouvrage = new Ouvrage(
						res.getInt("idOuvrage"),
						res.getString("lieux"),
						res.getString("titre"),
						res.getInt("nbPage"),
						res.getString("annee"),
						res.getString("commentaire"),
						res.getString("imprimeur"),
						res.getString("editeur"),
						res.getString("auteurNom"),
						res.getString("auteurPr�nom"),
						res.getString("auteurPseudo")
				);
				ouvrages.add(ouvrage);
			}
		} catch (Exception e) {e.printStackTrace();}
		return ouvrages;
	}
	
	/**
	 * Renvoie la premi�re page d'un ouvrage, en r�cup�rant les donn�es dans un
	 * @param o ID de l'ouvrage � afficher
	 * @see Connection#createStatement()
	 * @see Statement#executeQuery(String)
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(String)
	 * @see ResultSet#getString(String)
	 * @see ResultSet#getBoolean(String)
	 * @see Statement#close()
	 * @return Premi�re page de l'ouvrage ayant comme ID o, ou null si aucun r�sultat de la requ�te
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public Page getPage(int o) throws SQLException {
		Statement stmt = con.createStatement();
		String sql = "SELECT * FROM Page WHERE numeroPage = 1 AND idOuvrage = " + o;
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			String s = rs.getString("cheminImage");
			int i = rs.getInt("idPage");
			boolean b = rs.getBoolean("Analyse");
			return new Page(i, s, 1, b, o);
		}
		
		stmt.close();
		return null; 
	}
	
	/**
	 * Permet de r�cup�rer la page pr�c�dant ou suivant la page actuelle, en r�cup�rant les donn�es dans la BD
	 * @param p Num�ro de la page actuelle
	 * @param o ID de l'ouvrage � afficher
	 * @param direction Permet de choisir si l'on doit afficher la page pr�c�dente ou suivante
	 * @see Connection#createStatement()
	 * @see Statement#executeQuery(String)
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(String)
	 * @see ResultSet#getString(String)
	 * @see ResultSet#getBoolean(String)
	 * @see Statement#close()
	 * @return Premi�re page de l'ouvrage ayant comme ID o, ou null si aucun r�sultat de la requ�te
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public Page getPage(int p, int o, String direction) throws SQLException {
		if (direction == "G") p--;
		else p++;
		if (p < 0) p = 0;
		Statement stmt = con.createStatement();
		String sql = "SELECT * FROM Page WHERE numeroPage = " + p + " AND idOuvrage = " + o;
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			String s = rs.getString("cheminImage");
			int i = rs.getInt("idPage");
			boolean b = rs.getBoolean("Analyse");
			return new Page(i, s, p, b, o);
		}
		
		stmt.close();
		return null; 
	}
	
	/**
	 * Permet de renvoyer une page pr�cise d'un ouvrage en r�cup�rant les donn�es
	 * @param p Num�ro de la page chosie pour �tre affich�e
	 * @param o ID de l'ouvrage � afficher
	 * @see Connection#createStatement()
	 * @see Statement#executeQuery(String)
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(String)
	 * @see ResultSet#getString(String)
	 * @see ResultSet#getBoolean(String)
	 * @see Statement#close()
	 * @return Page p de l'ouvrage ayant comme ID o, ou null si aucun r�sultat de la requ�te
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public Page getPage(int p, int o) {
		String sql = "SELECT * FROM Page WHERE numeroPage = ? AND idOuvrage = ?";
		PreparedStatement pstmt = null;
		Page page = null;
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,p);
			pstmt.setInt(2, o);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String s = rs.getString("cheminImage");
				int i = rs.getInt("idPage");
				boolean b = rs.getBoolean("Analyse");
				page = new Page(i, s, p, b, o);
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return page;
	}

	/**
	 * Permet de r�cup�rer les donn�es d'un ouvrage
	 * @param o ID de l'ouvrage dont on d�sire les donn�es
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#executeQuery(String)
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(String)
	 * @see ResultSet#getString(String)
	 * @see Statement#close()
	 * @return Ouvrage o dont on d�sire les donn�es, ou null si aucun r�sultat de la requ�te
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public Ouvrage getOuvrage(int o) {
		try {
			PreparedStatement pstmt = con.prepareStatement("SELECT idOuvrage,titre,lieux,nbPage,annee,commentaire," +
					"(SELECT I.nom FROM Imprimeur I WHERE I.idImprimeur=O.idImprimeur) AS imprimeur," +
					"(SELECT E.nom FROM Editeur E WHERE E.idEditeur=O.idEditeur) AS editeur, " +
					"(SELECT A.nom FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurNom, " +
					"(SELECT A.prenom FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurPr�nom, " +
					"(SELECT A.pseudonyme FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurPseudo " +
					"FROM Ouvrage O WHERE idOuvrage = ?");
			pstmt.setInt(1, o);
			ResultSet res = pstmt.executeQuery();
			res.next();
			Ouvrage ouvrage = new Ouvrage(
					res.getInt("idOuvrage"),
					res.getString("lieux"),
					res.getString("titre"),
					res.getInt("nbPage"),
					res.getString("annee"),
					res.getString("commentaire"),
					res.getString("imprimeur"),
					res.getString("editeur"),
					res.getString("auteurNom"),
					res.getString("auteurPr�nom"),
					res.getString("auteurPseudo"));
			pstmt.close();
			return ouvrage;
		}

		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return null;
		}
	}

	/**
	 * Permet de renvoyer la liste des ouvrages respectant une recherche filtr�e par
	 * Titre, Auteur, Imprimeur, Editeur, Ann�e ou Lieu
	 * @param search Contenu de la recherche
	 * @param searchBy Filtre utilis� pour la recherche
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#executeQuery(String)
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(String)
	 * @see ResultSet#getString(String)
	 * @see ResultSet#getBoolean(String)
	 * @see PreparedStatement#close()
	 * @return Liste des ouvrages ne contenant pas ceux ne respectant le filtre
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public List<Ouvrage> chercherOuvrage(String search, String searchBy) {
		List<Ouvrage> ouvrages = new ArrayList<Ouvrage>();
		String[] filters = {"titre","auteurNom","auteurPr�nom","auteurPseudo","imprimeur","editeur","annee","lieux"};
		if (List.of(filters).contains(searchBy)) {
			String sql = "SELECT idOuvrage,titre,lieux,nbPage,annee,commentaire, " +
					"(SELECT I.nom FROM Imprimeur I WHERE I.idImprimeur=O.idImprimeur) AS imprimeur, " +
					"(SELECT E.nom FROM Editeur E WHERE E.idEditeur=O.idEditeur) AS editeur, " +
					"(SELECT A.nom FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurNom, " +
					"(SELECT A.prenom FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurPr�nom, " +
					"(SELECT A.pseudonyme FROM Auteur A WHERE A.idAuteur=O.idAuteur) AS auteurPseudo " +
					"FROM Ouvrage O HAVING "+ searchBy +" LIKE ?";
			try {
				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setString(1, "%"+search+"%");
				ResultSet res = pstmt.executeQuery();
				while (res.next()) {
					Ouvrage ouvrage = new Ouvrage(
							res.getInt("idOuvrage"),
							res.getString("lieux"),
							res.getString("titre"),
							res.getInt("nbPage"),
							res.getString("annee"),
							res.getString("commentaire"),
							res.getString("imprimeur"),
							res.getString("editeur"),
							res.getString("auteurNom"),
							res.getString("auteurPr�nom"),
							res.getString("auteurPseudo")
					);
					ouvrages.add(ouvrage);
				}
				
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(sql);
			}
			
			return ouvrages;
		}
		
		return ouvrages;
	}
	
	/**
	 * Supprime un ouvrage de la base de donn�es
	 * @param o ID de l'ouvrage � supprimer
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setInt(int, int)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#close()
	 * @see GestionOuvrage#afficherMenu()
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public void supprimerOuvrage(int o) {
		try {
			String sql = "DELETE FROM Caracteriser WHERE idComposante IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=?))";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.execute();
			pstmt.close();
			String sql2 = "DELETE FROM Ressembler WHERE idComposante IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=?)) OR idComposante_Ressembler IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=?))";
			pstmt = con.prepareStatement(sql2);
			pstmt.setInt(1, o);
			pstmt.setInt(2, o);
			pstmt.execute();
			pstmt.close();
			sql = "DELETE FROM Composante WHERE idPage IN (SELECT idPage FROM Page WHERE idOuvrage=?)";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.execute();
			pstmt.close();
			sql = "DELETE FROM Page WHERE idOuvrage = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.execute();
			pstmt.close();
			sql = "DELETE FROM Ouvrage WHERE idOuvrage = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.execute();
			pstmt.close();
		}
		
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
		}
	}
	
	/**
	 * Supprime la page s�lectionn�e
	 * @param p Num�ro de la page � supprimer
	 * @param o ID de l'ouvrage contenant la page � supprimer
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setInt(int, int)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#close()
	 * @see GestionOuvrage#getPage(int, int, String)
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return renvoie True si la suppression s'est bien pass�, False sinon
	 */
	public boolean supprimerPage(int p, int o) {
		try {
			String sql = "DELETE FROM Caracteriser WHERE idComposante IN (SELECT idComposante FROM Page WHERE idPage=" +
					"(SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?))";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.setInt(2, p);
			pstmt.execute();
			pstmt.close();
			String sql2 = "DELETE FROM Ressembler WHERE idComposante IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?)) OR idComposante_Ressembler IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?))";
			pstmt = con.prepareStatement(sql2);
			pstmt.setInt(1, o);
			pstmt.setInt(2, p);
			pstmt.setInt(3, o);
			pstmt.setInt(4, p);
			pstmt.execute();
			pstmt.close();
			sql = "DELETE FROM Composante WHERE idPage IN (SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?)";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.setInt(2,p);
			pstmt.execute();
			pstmt.close();
			String sql3 = "DELETE FROM Page WHERE idOuvrage = ? AND numeroPage = ?";
			pstmt = con.prepareStatement(sql3);
			pstmt.setInt(1, o);
			pstmt.setInt(2,p);
			pstmt.execute();
			pstmt.close();
			return true;
		}
		
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Permet d'ajouter une page puis de l'afficher
	 * @param cI Chemin de l'image
	 * @param p Num�ro de la page que l'on veut ajouter
	 * @param o ID de l'ouvrage auquel on veut ajouter la page
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#setInt(int, int)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#close()
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return Affichage de la page nouvellement cr��e, ou null si la requ�te SQL est �choue
	 */
	public boolean ajouterPage(String cI, int p, int o) {
		try {
			if (getPage(p,o) == null) {
				String sql = "INSERT INTO Page VALUES(?, ?, ?, false, ?)";
				PreparedStatement pstmt = con.prepareStatement(sql);
				pstmt.setNull(1,1);
				pstmt.setString(2,cI);
				pstmt.setInt(3,p);
				pstmt.setInt(4,o);
				pstmt.execute();
				pstmt.close();
				return true;
			} else return false;
		}
		
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return false;
		}
	}

	/**
	 * Permet d'ajouter un ouvrage
	 * @param titre Titre de l'ouvrage
	 * @param lieux Lieu de provenance de l'ouvrage
	 * @param nbPages Nombre de pages de l'ouvrage
	 * @param annee Ann�e d'�criture de l'ouvrage
	 * @param commentaire Pr�cisions apport�es sur l'ouvrage
	 * @param idImp ID de l'imprimeur de l'ouvrage
	 * @param idEdit ID de l'�diteur de l'ouvrage
	 * @param idAuteur ID de l'auteur de l'ouvrage
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#setInt(int, int)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#close()
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return Affichage du menu, ou null si la requ�te SQL est �choue
	 */
	public List<Ouvrage> ajouterOuvrage(String titre, String lieux, int nbPages, String annee, String commentaire,
			int idImp, int idEdit, int idAuteur) {
		try {
			String sql = "INSERT INTO Ouvrage VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setNull(1,1);
			pstmt.setString(2, titre);
			pstmt.setString(3, lieux);
			pstmt.setInt(4, nbPages);
			pstmt.setString(5, annee);
			pstmt.setString(6, commentaire);
			pstmt.setInt(7, idImp);
			pstmt.setInt(8, idEdit);
			pstmt.setInt(9, idAuteur);
			pstmt.execute();
			pstmt.close();
			return (this.afficherMenu());
		}

		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Permet de modifier l'image enregistr� pour une page
	 * @param cI Nouveau chemin de l'image
	 * @param p Num�ro de la page � modifier
	 * @param o Ouvrage contenant la page � modifier
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#setInt(int, int)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#close()
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return Affichage du menu, ou null si la requ�te SQL est �choue
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public boolean modifPage(String cI, int p, int o) throws SQLException {
		try {
			String sql = "DELETE FROM Caracteriser WHERE idComposante IN (SELECT idComposante FROM Page WHERE idPage=" +
					"(SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?))";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, o);
			pstmt.setInt(2, p);
			pstmt.execute();
			pstmt.close();
			String sql2 = "DELETE FROM Ressembler WHERE idComposante IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?)) OR idComposante_Ressembler IN (SELECT idComposante FROM Page WHERE idPage IN " +
					"(SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?))";
			pstmt = con.prepareStatement(sql2);
			pstmt.setInt(1, o);
			pstmt.setInt(2, p);
			pstmt.setInt(3, o);
			pstmt.setInt(4, p);
			pstmt.execute();
			pstmt.close();
			String sql3 = "DELETE FROM Composante WHERE idPage IN (SELECT idPage FROM Page WHERE idOuvrage=? AND numeroPage=?)";
			pstmt = con.prepareStatement(sql3);
			pstmt.setInt(1, o);
			pstmt.setInt(2, p);
			pstmt.execute();
			pstmt.close();
			String sql4 = "UPDATE Page SET cheminImage = ? WHERE idOuvrage = ? AND numeroPage = ?";
			pstmt = con.prepareStatement(sql4);
			pstmt.setString(1, cI);
			pstmt.setInt(2, o);
			pstmt.setInt(3, p);
			pstmt.execute();
			pstmt.close();
			String sql5 = "UPDATE Page SET analyse = 0 WHERE idOuvrage = ? AND numeroPage = ?";
			pstmt = con.prepareStatement(sql5);
			pstmt.setInt(1, o);
			pstmt.setInt(2, p);
			pstmt.execute();
			pstmt.close();
			return true;
		}
		
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Permet de modifier la valeur d'un attribut enregistr� pour un ouvrage
	 * @param idOuvrage ID de l'ouvrage � modifier
	 * @param auteur Nouvel ID de l'auteur de l'ouvrage
	 * @param imp Nouvel ID de l'imprimeur de l'ouvrage
	 * @param edit Nouvel ID de l'�diteur de l'ouvrage
	 * @param annee Nouvelle ann�e de r�dactiion de l'ouvrage
	 * @param nbPages Nouveau nombre de pages de l'ouvrage
	 * @param lieu Nouveau lieu de l'ouvrage
	 * @param commentaire Nouveau commenataire de l'ouvrage
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#setInt(int, int)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#close()
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @throws SQLException Si une erreur lors de la requ�te avec la BD survient
	 */
	public void modifOuvrage(int idOuvrage, String titre,int auteur, int imp, int edit, String annee,
			int nbPages, String lieu, String commentaire) {
		try {
			String sql = "UPDATE Ouvrage SET titre = ?, lieux = ?, nbPage = ?, annee = ?, commentaire = ?, " +
		"idImprimeur = ?, idEditeur = ?, idAuteur = ? WHERE idOuvrage = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, titre);
			pstmt.setString(2, lieu);
			pstmt.setInt(3, nbPages);
			pstmt.setString(4, annee);
			pstmt.setString(5, commentaire);
			pstmt.setInt(6, imp);
			pstmt.setInt(7, edit);
			pstmt.setInt(8, auteur);
			pstmt.setInt(9, idOuvrage);
			pstmt.execute();
			pstmt.close();
		}
		
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
		}
	}
	
	/**
	 * Permet de r�cup�rer l'ID d'un imprimeur dans la base de donn�es
	 * S'il il n'existe pas, le cr�er puis relancer la m�thode
	 * @param nom Nom de l'imprimeur
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#executeQuery()
	 * @see PreparedStatement#close()
	 * @see ResultSet#isBeforeFirst()
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(int)
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return ID de l'imprimeur dans la base de donn�es
	 */
	public int getImprimeur(String nom) {
		try {
			String sql = "SELECT idImprimeur FROM Imprimeur WHERE nom = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, nom);
			ResultSet rs = pstmt.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				return rs.getInt("idImprimeur");
			}

			else {
				String sql2 = "INSERT INTO Imprimeur VALUES(null, ?)";
				PreparedStatement pstmt2 = con.prepareStatement(sql2);
				pstmt2.setString(1, nom);
				pstmt2.execute();
				pstmt2.close();
				return getImprimeur(nom);
			}
		}

		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return (Integer) null;
			}
		}
	
	/**
	 * Permet de r�cup�rer l'ID d'un �diteur dans la base de donn�es
	 * S'il il n'existe pas, le cr�er puis relancer la m�thode
	 * @param nom Nom de l'�diteur
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#executeQuery()
	 * @see PreparedStatement#close()
	 * @see ResultSet#isBeforeFirst()
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(int)
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return ID de l'�diteur dans la base de donn�es
	 */
	public int getEditeur(String nom) {
		try {
			String sql = "SELECT idEditeur FROM Editeur WHERE nom = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, nom);
			ResultSet rs = pstmt.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				return rs.getInt("idEditeur");
			}
		
			else {
				String sql2 = "INSERT INTO Editeur VALUES(null, ?)";
				PreparedStatement pstmt2 = con.prepareStatement(sql2);
				pstmt2.setString(1, nom);
				pstmt2.execute();
				pstmt2.close();
				return getEditeur(nom);
			}
		}
	
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return (Integer) null;
		}
	}
	
	/**
	 * Permet de r�cup�rer l'ID d'un auteur dans la base de donn�es
	 * S'il il n'existe pas, le cr�er puis relancer la m�thode
	 * @param nom Nom de l'auteur
	 * @param pr�nom Pr�nom de l'auteur
	 * @param pseudo Pseudonyme de l'auteur
	 * @see Connection#prepareStatement(String)
	 * @see PreparedStatement#setString(int, String)
	 * @see PreparedStatement#execute()
	 * @see PreparedStatement#executeQuery()
	 * @see PreparedStatement#close()
	 * @see ResultSet#isBeforeFirst()
	 * @see ResultSet#next()
	 * @see ResultSet#getInt(int)
	 * @see System#err
	 * @see SQLException#printStackTrace()
	 * @return ID de l'auteur dans la base de donn�es
	 */
	public int getAuteur(String nom, String pr�nom, String pseudo) {
		try {
			String sql = "SELECT idAuteur FROM Auteur WHERE nom = ? AND prenom = ? AND pseudonyme = ?";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, nom);
			pstmt.setString(2, pr�nom);
			pstmt.setString(3, pseudo);
			ResultSet rs = pstmt.executeQuery();
			if (rs.isBeforeFirst()) {
				rs.next();
				return rs.getInt("idAuteur");
			}
	
			else {
				String sql2 = "INSERT INTO Auteur VALUES(null, ?, ?, ?)";
				PreparedStatement pstmt2 = con.prepareStatement(sql2);
				pstmt2.setString(1, nom);
				pstmt2.setString(2, pr�nom);
				pstmt2.setString(3, pseudo);
				pstmt2.execute();
				pstmt2.close();
				return getAuteur(nom, pr�nom, pseudo);
			}
		}
	
		catch(SQLException E) {
			System.err.println("Erreur li�e � la requ�te SQL !");
			E.printStackTrace();
			return (Integer) null;
		}
	}
}