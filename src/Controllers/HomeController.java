package Controllers;

import Modeles.Ouvrage;
import Modeles.GestionOuvrage;
import Vues.HomeVue;
import java.util.List;

/**
 * @author MAILLY Lucas
 */
public class HomeController {

    private final HomeVue homeVue;

    public HomeController(HomeVue homeVue) {
        this.homeVue = homeVue;
    }

    private static final GestionOuvrage dao = new GestionOuvrage(
            "jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
            "root",
            "");

    public void afficheMenu() {
        homeVue.clear();
        List<Ouvrage> ouvrages = dao.afficherMenu();
        for (Ouvrage o : ouvrages) {
            homeVue.AjouteOuvrage(o.getTitre(),o.getAuteurNom(),o.getAuteurPrenom(),o.getAuteurPseudo(),o.getImprimeur(),o.getEditeur(),o.getAnnee(),o.getNbPages(),o.getLieux(),o.getIdOuvrage());
        }
    }

    public void chercherOuvrage(String search, String searchBy) {
        homeVue.clear();
        List<Ouvrage> ouvrages;
        if (!search.equals("")) {
            ouvrages = dao.chercherOuvrage(search, searchBy);
        } else {
            ouvrages = dao.afficherMenu();
        }
        
        for (Ouvrage o : ouvrages){
            homeVue.AjouteOuvrage(o.getTitre(),o.getAuteurNom(),o.getAuteurPrenom(),o.getAuteurPseudo(),o.getImprimeur(),o.getEditeur(),o.getAnnee(),o.getNbPages(),o.getLieux(),o.getIdOuvrage());
        }
    }
}