package Controllers;

import Modeles.Composante;
import Modeles.ImgViewer;
import Modeles.Page;

import java.util.ArrayList;

/**
 * @author MAILLY Lucas
 */
public class ImgViewerController {

    private final ImgViewer imgViewer;
    private String imagePath;

    public ImgViewerController(){
        imgViewer = new ImgViewer();
    }

    public ArrayList<Composante> getMeasures(){
        return imgViewer.getComposantes();
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void changePage(Page page){
        imgViewer.changePage(page);
    }
}
