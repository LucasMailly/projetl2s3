package Controllers;

import java.sql.SQLException;

import Modeles.GestionOuvrage;
import Modeles.Ouvrage;
import Vues.HomeVue;
import Vues.ModifPageVue;
import Vues.SupprimerOuvrageVue;

/**
 * @author Mathis Gaborieau
 *
 */
public class SupprimerOuvrageController {
	//Attributs
	private HomeVue homeVue;
	private int idOuvrage;
	private GestionOuvrage dao = new GestionOuvrage(
			"jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
	        "root",
	        "");
	
	//Constructeurs
	public SupprimerOuvrageController(int idOuvrage, HomeVue homeVue) {
		this.homeVue = homeVue;
		this.idOuvrage = idOuvrage;	
	}
		
	//M�thodes
	public void supprimerOuvrage() throws SQLException {
	    dao.supprimerOuvrage(idOuvrage);   
	}
}
