package Controllers;

import Modeles.GestionOuvrage;
import Vues.AjoutOuvrageVue;
import Vues.HomeVue;

/**
 * Classe AjoutOuvrageController
 * @author LAURIN Victor
 */
public class AjoutOuvrageController {
	//Attributs
	private final AjoutOuvrageVue ajoutOuvrageVue;
	private final GestionOuvrage dao = new GestionOuvrage(
            "jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
            "root",
            "");
	
	//Constructeurs

	/**
	 * Constructeur AjoutOuvrageController initialis� avec sa vue parent.
	 * Cr�er une vue AjoutOuvrageVue
	 * @param homeVue Vue parent
	 * @see HomeVue
	 * @see AjoutOuvrageVue
	 */
	public AjoutOuvrageController(HomeVue homeVue) {
		ajoutOuvrageVue = new AjoutOuvrageVue(this, homeVue);
	}
	
	//M�thodes

	/**
	 * Appelle la m�thode ajouterOuvrage de la classe GestionOuvrage
	 * en transformant les param�tres li�es � l'imprimeur, l'�diteur et l'auteur
	 * en un id correspond � chacun de ces trois param�tres en base de donn�es.
	 * @param titre Titre de l'ouvrage
	 * @param lieux Lieu d'�criture de l'ouvrage
	 * @param nbPages Nombre de page de l'ouvrage
	 * @param annee ann�e d'�criture de l'ouvrage
	 * @param commentaire commentaire de l'ouvrage
	 * @param imp Imprimeur de l'ouvrage
	 * @param edit Editeur de l'ouvrage
	 * @param auteurNom Nom de l'auteur de l'ouvrage
	 * @param auteurPrenom Pr�nom de l'auteur de l'ouvrage
	 * @param auteurPseudo Pseudo de l'auteur de l'ouvrage
	 * @see GestionOuvrage#ajouterOuvrage(String, String, int, String, String, int, int, int)
	 */
	public void ajouterOuvrage(String titre, String lieux, int nbPages, String annee, String commentaire, 
			String imp, String edit, String auteurNom, String auteurPrenom, String auteurPseudo) {
		int idImp = dao.getImprimeur(imp);
		int idEdit = dao.getEditeur(edit);
		int idAuteur = dao.getAuteur(auteurNom, auteurPrenom, auteurPseudo);
		dao.ajouterOuvrage(titre, lieux, nbPages, annee, commentaire, idImp, idEdit, idAuteur);
	}

	/**
	 * @return La vue AjoutOuvrageVue correspondante � cette classe AjoutOuvrageController.
	 * @see AjoutOuvrageVue
	 */
	public AjoutOuvrageVue getAjoutOuvrageVue() {
		return ajoutOuvrageVue;
	}
}
