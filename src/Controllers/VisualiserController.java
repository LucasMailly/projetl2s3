package Controllers;

import Modeles.*;
import Vues.VisualiserVue;

import java.util.List;

/**
 * Classe VisualiserController
 * @author LAURIN Victor
 */
public class VisualiserController {
	//Attributs
	private VisualiserVue visualiserVue;
	private Ouvrage ouvrage;
	private Page page;
	private int numPage = 1;
	private ImgViewerController imgViewerCtrl;
	private GestionOuvrage daoOuvrage = new GestionOuvrage(
            "jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
            "root",
            "");
	private GestionComposante daoComposante = new GestionComposante(
			"jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
			"root",
			"");

	//Constructeurs
	public VisualiserController(int idOuvrage, ImgViewerController imgViewerCtrl) {
		this.imgViewerCtrl = imgViewerCtrl;
		visualiserVue = new VisualiserVue(this, imgViewerCtrl);
		this.ouvrage = daoOuvrage.getOuvrage(idOuvrage);
		page = daoOuvrage.getPage(numPage, idOuvrage);
	}
	
	//Méthodes
	public void initialiseOuvrage() {
		visualiserVue.ajouterInfosOuvrage(
				ouvrage.getLieux(), ouvrage.getTitre(), ouvrage.getNbPages(),
				ouvrage.getAnnee(), ouvrage.getCommentaire(), ouvrage.getImprimeur(),
				ouvrage.getEditeur(), ouvrage.getAuteurNom(), ouvrage.getAuteurPrenom(),
				ouvrage.getAuteurPseudo());
	}

	public void initialisePage() {
		if (page != null) {
			updatePage();
		} else {
			visualiserVue.clearImage();
		}
		updateNumPage();
	}
	
	public void modifierOuvrage(String titre, String auteurNom, String auteurPrénom, String auteurPseudo,
			String imp, String edit, String annee, 
			int nbPages, String lieu, String commentaire) {
		int idImp = daoOuvrage.getImprimeur(imp);
		int idEdit = daoOuvrage.getEditeur(edit);
		int idAuteur = daoOuvrage.getAuteur(auteurNom, auteurPrénom, auteurPseudo);
		daoOuvrage.modifOuvrage(ouvrage.getIdOuvrage(), titre,idAuteur, idImp, idEdit, annee, nbPages, lieu, commentaire);
	}

	public void modifierCC(Composante cc, String transcription,String type,String police,int largeur,int hauteur,int xStart, int yStart,String commCC,String keywords) {
		int idComposante = cc.getIdComposante();
		if (cc.getTranscription()!=transcription)
			daoComposante.createTranscription(transcription);
		if (cc.getPolice()!=police)
			daoComposante.createNomPolice(police);
		if (cc.getType()!=type)
			daoComposante.createNomComposante(type);
		if (cc.getKeywords()!=keywords){
			List<String> newKeywords = List.of(keywords.split("\n"));
			if (cc.getKeywords()!=null){
				List<String> oldKeywords = List.of(cc.getKeywords().split("\n"));
				for (String kw : newKeywords){
					if(!oldKeywords.contains(kw)){
						daoComposante.createKeyword(kw);
						daoComposante.addKeyword(idComposante,kw);
					}
				}
				for (String kw : oldKeywords){
					if(!newKeywords.contains(kw)){
						daoComposante.removeKeyword(idComposante,kw);
					}
				}
			}else{
				for (String kw : newKeywords){
					daoComposante.createKeyword(kw);
					daoComposante.addKeyword(idComposante,kw);
				}
			}
		}
		daoComposante.modifierCC(idComposante, transcription, type, police,largeur,hauteur,
				xStart,yStart, commCC);
		Composante selectedCC = new Composante(
				idComposante,
				xStart,
				yStart,
				largeur,
				hauteur,
				commCC,
				cc.getIdPage(),
				police,
				type,
				transcription,
				keywords
		);
		visualiserVue.setComposante(selectedCC);
		updatePage();
	}
	
	public int getIdOuvrage() {
		return ouvrage.getIdOuvrage();
	}

	public VisualiserVue getVisualiserVue() {
		return this.visualiserVue;
	}

	public boolean previousPage() {
		if (numPage!=1){
			numPage--;
			updatePage();
			if(page==null)
				visualiserVue.clearImage();
			updateNumPage();
			return true;
		}else{
			return false;
		}
	}

	public boolean nextPage(){
		if (numPage!=ouvrage.getNbPages()){
			numPage++;
			updatePage();
			if (page==null)
				visualiserVue.clearImage();
			updateNumPage();
			return true;
		}else{
			return false;
		}
	}

	public void updatePage() {
		page = daoOuvrage.getPage(numPage, ouvrage.getIdOuvrage());
		imgViewerCtrl.changePage(page);
		if (page!=null)
			visualiserVue.changeImage(page.getCheminImage());
		else
			visualiserVue.changeImage("");
	}

	public void updateNumPage(){
		visualiserVue.setNumPage(numPage);
	}
	
	public int getNumPage() {
		return numPage;
	}

	public Page getPage(){
		return page;
	}
}