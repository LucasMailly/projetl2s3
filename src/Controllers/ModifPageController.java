package Controllers;

import java.sql.SQLException;

import Modeles.GestionOuvrage;
import Modeles.Ouvrage;
import Vues.ModifPageVue;
import Vues.VisualiserVue;

/**
 * Classe AjoutPageController
 * @author LAURIN Victor
 */
public class ModifPageController {
	//Attributs
	private VisualiserVue visualiserVue;
	private ModifPageVue modifPageVue;
	private Ouvrage ouvrage;
	private int numPage;
	private GestionOuvrage dao = new GestionOuvrage(
            "jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
            "root",
            "");
	
	//Constructeurs
	public ModifPageController(int idOuvrage, VisualiserVue visualiserVue, int numPage) {
		this.visualiserVue = visualiserVue;
		modifPageVue = new ModifPageVue(this, visualiserVue);
		this.ouvrage = dao.getOuvrage(idOuvrage);
		this.numPage = numPage;
	}

	//M�thodes
	public String modifierPage(String cI) throws SQLException {
		if (!dao.modifPage(cI, numPage, ouvrage.getIdOuvrage()))
			return "Erreur lors de l'ajout.";
		else
			return "";
	}

	public ModifPageVue getModifPageVue() {
		return modifPageVue;
	}
}
