package Controllers;

import Modeles.GestionOuvrage;
import Modeles.Ouvrage;
import Vues.AjoutPageVue;
import Vues.VisualiserVue;

/**
 * Classe AjoutPageController
 * @author LAURIN Victor
 */
public class AjoutPageController {
	//Attributs
	private final AjoutPageVue ajoutPageVue;
	private final Ouvrage ouvrage;
	private final GestionOuvrage dao = new GestionOuvrage(
            "jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
            "root",
            "");
	
	//Constructeurs
	public AjoutPageController(int idOuvrage, VisualiserVue visualiserVue) {
		ajoutPageVue = new AjoutPageVue(this, visualiserVue);
		this.ouvrage = dao.getOuvrage(idOuvrage);
	}

	//M�thodes

	/**
	 * Appelle la m�thode ajouterPage de la classe GestionOuvrage
	 * si le num�ro de page est coh�rent avec le nombre de page de l'ouvrage.
	 * @param cI chemin de l'image de la page
	 * @param numPage num�ro de la page
	 * @return Une phrase d'erreur si mauvais num�ro de page ou ajouterPage de GestionOuvrage renvoie false
	 */
	public String ajouterPages(String cI, int numPage) {
		if (numPage>ouvrage.getNbPages())
			return "Le nombre de pages maximum pour cet ouvrage est de "+numPage+" page(s)";
		else if (!dao.ajouterPage(cI, numPage, ouvrage.getIdOuvrage()))
			return "Erreur lors de l'ajout. V�rifier que la page "+ numPage +" n'existe pas d�j�.";
		else
			return "";
	}

	/**
	 * @return La vue AjoutPageVue correspondante � cette classe AjoutPageController.
	 */
	public AjoutPageVue getAjoutPageVue() {
		return ajoutPageVue;
	}
}
