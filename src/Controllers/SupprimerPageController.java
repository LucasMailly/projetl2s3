package Controllers;

import Modeles.GestionOuvrage;
import Vues.VisualiserVue;

public class SupprimerPageController {
	//Attributs
	private VisualiserVue visualiserVue;
	private int idOuvrage;
	private int numPage;
	private GestionOuvrage dao = new GestionOuvrage(
			"jdbc:mysql://localhost/projetL2S3?serverTimezone=UTC",
	        "root",
	        "");
	
	//Constructeurs
	public SupprimerPageController(int idOuvrage, int numPage, VisualiserVue visualiserVue) {
		this.visualiserVue = visualiserVue;
		this.idOuvrage = idOuvrage;
		this.numPage = numPage;
	}
	
	//M�thodes
	public void supprimerPage() {
		dao.supprimerPage(numPage, idOuvrage);
	}
}
