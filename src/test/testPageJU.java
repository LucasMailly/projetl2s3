package test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Modeles.Page;

@RunWith(value=Parameterized.class)
public class testPageJU {
	//Attributs
	private int idPage;
	private String cheminImage;
	private int numeroPage;
	private boolean analyse;
	private int idOuvrage;
	
	//Constructeurs
	public testPageJU(int idPage, String cheminImage, int numeroPage, boolean analyse, int idOuvrage) {
		this.idPage = idPage;
		this.cheminImage = cheminImage;
		this.numeroPage = numeroPage;
		this.analyse = analyse;
		this.idOuvrage = idOuvrage;
	}
	
	//M�thodes
	@Parameters //Param�tres de test
	public static Collection<Object[]> data() {
	    return Arrays.asList(
	        new Object[][]
	        { {2, "test.png", 4, true, 5}, {15, "essai.png", 47, false, 12} }
	    );
	}
	
	@Test //M�thode de test
	public void testGetIdPage() {
		System.out.println("Test du renvoie de l'ID de la page avec : " + this.idPage);
		Page p = new Page(this.idPage, this.cheminImage, this.numeroPage, this.analyse, this.idOuvrage);
		assertEquals(this.idPage, p.getIdPage());
	}
	
	@Test
	public void testGetCheminImage() {
		System.out.println("Test du renvoie du chemin de l'image de la page avec : " + this.cheminImage);
		Page p = new Page(this.idPage, this.cheminImage, this.numeroPage, this.analyse, this.idOuvrage);
		assertEquals(this.cheminImage, p.getCheminImage());
	}
	
	@Test
	public void testGetNumeroPage() {
		System.out.println("Test du renvoie du num�ro de la page avec : " + this.numeroPage);
		Page p = new Page(this.idPage, this.cheminImage, this.numeroPage, this.analyse, this.idOuvrage);
		assertEquals(this.numeroPage, p.getNumeroPage());
	}
	
	@Test
	public void testGetIsAnalyse() {
		System.out.println("Test pour savoir si l'image de la page est analys�e avec : " + this.cheminImage);
		Page p = new Page(this.idPage, this.cheminImage, this.numeroPage, this.analyse, this.idOuvrage);
		assertEquals(this.analyse, p.isAnalyse());
	}
	
	@Test
	public void testGetIdOuvrage() {
		System.out.println("Test du renvoie de l'ID de l'ouvrage avec : " + this.idOuvrage);
		Page p = new Page(this.idPage, this.cheminImage, this.numeroPage, this.analyse, this.idOuvrage);
		assertEquals(this.idOuvrage, p.getIdOuvrage());
	}
	
	@Test
	public void testSetAnalyse() {
		System.out.println("Test du changement de l'�tat d'analyse de l'image de la page avec : " + !this.analyse);
		Page p = new Page(this.idPage, this.cheminImage, this.numeroPage, this.analyse, this.idOuvrage);
		p.setAnalyse(!this.analyse);
		assertEquals(!this.analyse, p.isAnalyse());
	}
}
